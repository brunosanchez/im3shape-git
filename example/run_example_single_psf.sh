#!/bin/bash

# Number of galaxies to run on
N=1
N_FIRST=1

# Remove output files
rm example_output.c.txt
rm example_output.py.txt

echo "======================================================"
echo "Example and comparison of im3shape C and Python driver"
echo "======================================================"
echo "Run im3shape on first $N galaxies of example:         "
echo "------------------------------------------------------"
START=$(date +%s)
../bin/im3shape example.ini image.fits catalog.txt psf.fits example_output.c.txt $N_FIRST $N
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Time taken: $DIFF sec                                 "
echo "======================================================"
echo "Run im3shape.py on first $N galaxies of example       "
echo "------------------------------------------------------"
START=$(date +%s)
python ../python/im3shape.py example.ini image.fits catalog.txt psf.fits example_output.py.txt $N_FIRST $N
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Time taken: $DIFF sec                                 "
echo "======================================================"
echo "Comparison                                            "
echo "------------------------------------------------------"
# Extract values for comparison
like_c=`cat example_output.c.txt|awk '{sum+=$4} END {print sum}'` 
like_py=`cat example_output.py.txt|awk '{sum+=$4} END {print sum}'` 
e1_c=`cat example_output.c.txt|awk '{sum+=$7} END {print sum}'` 
e1_py=`cat example_output.py.txt|awk '{sum+=$7} END {print sum}'` 
e2_c=`cat example_output.c.txt|awk '{sum+=$8} END {print sum}'` 
e2_py=`cat example_output.py.txt|awk '{sum+=$8} END {print sum}'` 
echo "Cumulative likelihood, e1, e2"
echo "im3shape:     $like_c $e1_c $e2_c"
echo "im3shape.py:  $like_py $e1_py $e2_py"
