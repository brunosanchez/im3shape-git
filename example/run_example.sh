#!/bin/bash

# Number of galaxies to run on
N=10
N_FIRST=5

# Remove output files
rm -f example_output.c.txt
rm -f example_output.py.txt

echo "======================================================"
echo "Example and comparison of im3shape C and Python driver"
echo "======================================================"
echo "Run im3shape on first $N galaxies of example:         "
echo "------------------------------------------------------"
START=$(date +%s)
echo ../bin/im3shape example.ini image.fits catalog.txt psf.txt example_output.c.txt $N_FIRST $N
../bin/im3shape example.ini image.fits catalog.txt psf.txt example_output.c.txt $N_FIRST $N
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Time taken: $DIFF sec                                 "
echo "======================================================"
echo "Run im3shape.py on first $N galaxies of example       "
echo "------------------------------------------------------"
START=$(date +%s)
echo python ../bin/im3shape.py example.ini image.fits catalog.txt psf.txt example_output.py.txt $N_FIRST $N
python ../bin/im3shape.py example.ini image.fits catalog.txt psf.txt example_output.py.txt $N_FIRST $N
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Time taken: $DIFF sec                                 "
echo "======================================================"
echo "Comparison                                            "
echo "------------------------------------------------------"

function sum_col(){
    cat $1 | awk '{sum+=$'$2'} END {print sum}'
}

# Extract values for comparison
like_c=`sum_col example_output.c.txt 4`
e1_c=`sum_col example_output.c.txt 7`
e2_c=`sum_col example_output.c.txt 8`

like_py=`sum_col example_output.py.txt 24`
e1_py=`sum_col example_output.py.txt 3`
e2_py=`sum_col example_output.py.txt 4`


echo "Cumulative likelihood, e1, e2"
echo "im3shape:     $like_c $e1_c $e2_c"
echo "im3shape.py:  $like_py $e1_py $e2_py"
