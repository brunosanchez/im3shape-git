#ifndef _H_I3_XRAY
#define _H_I3_XRAY
#include "i3_xray_definition.h"
#include "i3_image.h"
#include "i3_data_set.h"
#include "i3_options.h"

//i3_flt i3_sersics_likelihood_old(i3_image * model_image, i3_sersics_parameter_set * p, i3_data_set * dataset);

//void i3_sersics_components(i3_sersics_parameter_set * p, i3_data_set * dataset, i3_image * image_dvc, i3_image * image_exp);


// void i3_xray_image(i3_xray_parameter_set * p, i3_data_set * dataset, i3_image * model_image);

// void i3_sersics_start(i3_sersics_parameter_set * start, i3_data_set * data_set, i3_options * options);

i3_flt i3_xray_likelihood(i3_image * model_image, i3_xray_parameter_set * parameters, i3_data_set * dataset);

void i3_xray_model_image(i3_xray_parameter_set * p, i3_data_set * dataset, i3_image * model_image);

// i3_sersics_parameter_set * i3_sersics_beermat_params(i3_sersics_parameter_set * p , i3_options * options);


// void i3_xray_get_image_info(i3_xray_parameter_set * params, i3_data_set * dataset,  i3_flt * image_args, i3_flt * image_info);

#endif
