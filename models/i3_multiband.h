#ifndef _H_i3_multiband
#define _H_i3_multiband
#include "i3_multiband_definition.h"
#include "i3_image.h"
#include "i3_data_set.h"
#include "i3_options.h"

#define MULTIband_AVAILABLE

double i3_multiband_model_exposure_tile_chi2(i3_multiband_parameter_set * p, i3_data_set * dataset, i3_image * tiled_image);



void i3_multiband_model_image_using_hankel_transform(i3_multiband_parameter_set * p, i3_data_set * dataset, i3_image * model_image, bool save_components, int exposure_index);
void i3_multiband_image(i3_multiband_parameter_set * p, i3_data_set * dataset, i3_image * model_image, i3_flt A, int proc_id, int verb);


void i3_multiband_start(i3_multiband_parameter_set * start, i3_data_set * data_set, i3_options * options);

i3_flt i3_multiband_likelihood(i3_image * model_image, i3_multiband_parameter_set * parameters, i3_data_set * dataset);

void i3_multiband_model_image(i3_multiband_parameter_set * p, i3_data_set * dataset, i3_image * model_image, int exposure_index);
void i3_multiband_model_image_save_components(i3_multiband_parameter_set * p, i3_data_set * dataset, i3_image * model_image, bool save_components, int exposure_index);

i3_multiband_parameter_set * i3_multiband_beermat_params(i3_multiband_parameter_set * p , i3_options * options);

void i3_multiband_beermat_mapping(i3_multiband_parameter_set * input, i3_multiband_parameter_set * output, i3_options * options);


void i3_multiband_get_image_info(i3_multiband_parameter_set * params, i3_data_set * dataset,  i3_flt * image_args, i3_flt * image_info);

#endif
