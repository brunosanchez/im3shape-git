name="quadratic_test"

parameters = ['x1','x2','x3','x4','x5']

types = {
	'x1':float,
	'x2':float,
	'x3':float,
	'x4':float,
	'x5':list,
}

widths = {
	'x1':0.1,
	'x2':0.1,
	'x3':0.1,
	'x4':0.1,
	'x5':[0.2,0.2,0.2]
}

starts = {
	'x1':0.0,
	'x2':0.0,
	'x3':0.0,
	'x4':0.0,
	'x5':[0.2,0.2,0.2]
	
}

min = {
	'x1':0.0,
	'x2':0.0,
	'x3':0.0,
	'x4':0.0,
	'x5':[-1.0,-1.0,-1.0]
	
}

max = {
	'x1':1.0,
	'x2':1.0,
	'x3':1.0,
	'x4':1.0,
	'x5':[1.0,1.0,1.0]
	
}


#This is optional
help =  {
	'x1':'Center of gaussian in dimension 1',
	'x2':'Center of gaussian  in dimension 2',
	'x3':'Center of gaussian  in dimension 3',
	'x4':'Center of gaussian  in dimension 4',
	'x5':'Unused test float parameter',
}


proposal="i3_covariance_matrix_proposal"
#derivative="i3_quadratic_test_likelihood_derivative"