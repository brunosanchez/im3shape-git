#include "i3_quadratic_test.h"
#include "i3_mcmc.h"
#include "math.h"
#include "i3_math.h"
#include "i3_model.h"

int i3_quadratic_test_number_calls=0;
int i3_quadratic_test_number_gradient_calls=0;

// FROM THE HEADER:
// #define QUADRATIC_TEST_TRUE_MEAN1 0.3
// #define QUADRATIC_TEST_TRUE_VAR1 (0.02*0.02)
// 
// #define QUADRATIC_TEST_TRUE_MEAN2 0.8
// #define QUADRATIC_TEST_TRUE_VAR2 (0.01*0.01)
// 
// #define QUADRATIC_TEST_TRUE_MEAN3 0.025
// #define QUADRATIC_TEST_TRUE_VAR3 (0.004*0.004)
// 
// #define QUADRATIC_TEST_TRUE_MEAN4 0.5
// #define QUADRATIC_TEST_TRUE_VAR4 (0.1*0.1)



i3_flt i3_quadratic_test_likelihood(i3_image * model_image, i3_quadratic_test_parameter_set * parameters, i3_data_set * dataset){
	/* Compute the likelihood probability of the supplied parameter set */
	/* This is a placeholder to test the MCMC */
	i3_quadratic_test_number_calls++;
	i3_flt chi2 = 0.0;

	chi2 += i3_pow(parameters->x1 - QUADRATIC_TEST_TRUE_MEAN1, 2)/QUADRATIC_TEST_TRUE_VAR1;
	chi2 += i3_pow(parameters->x2 - QUADRATIC_TEST_TRUE_MEAN2, 2)/QUADRATIC_TEST_TRUE_VAR2;
	chi2 += i3_pow(parameters->x3 - QUADRATIC_TEST_TRUE_MEAN3, 2)/QUADRATIC_TEST_TRUE_VAR3;
	chi2 += i3_pow(parameters->x4 - QUADRATIC_TEST_TRUE_MEAN4, 2)/QUADRATIC_TEST_TRUE_VAR4;
	chi2 += i3_pow(parameters->x5[0] - QUADRATIC_TEST_TRUE_MEAN5, 2)/QUADRATIC_TEST_TRUE_VAR5;
	chi2 += i3_pow(parameters->x5[1] - QUADRATIC_TEST_TRUE_MEAN5, 2)/QUADRATIC_TEST_TRUE_VAR5;
	chi2 += i3_pow(parameters->x5[2] - QUADRATIC_TEST_TRUE_MEAN5, 2)/QUADRATIC_TEST_TRUE_VAR5;

	return -chi2/2;
}


void i3_quadratic_test_likelihood_derivative(i3_image * model_image, i3_flt like, i3_data_set* data_set, i3_quadratic_test_parameter_set * x, i3_quadratic_test_parameter_set * xprime)
{
	i3_quadratic_test_number_gradient_calls++;
	xprime->x1 = -(x->x1 - QUADRATIC_TEST_TRUE_MEAN1)/QUADRATIC_TEST_TRUE_VAR1;
	xprime->x2 = -(x->x2 - QUADRATIC_TEST_TRUE_MEAN2)/QUADRATIC_TEST_TRUE_VAR2;
	xprime->x3 = -(x->x3 - QUADRATIC_TEST_TRUE_MEAN3)/QUADRATIC_TEST_TRUE_VAR3;
	xprime->x4 = -(x->x4 - QUADRATIC_TEST_TRUE_MEAN4)/QUADRATIC_TEST_TRUE_VAR4;
//	printf("Internal likelihood gradient: (%f,%f,%f,%f)\n",xprime->x1,xprime->x2,xprime->x3,xprime->x4);
}


