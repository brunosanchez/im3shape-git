#ifndef _H_I3_LOGSERSICS
#define _H_I3_LOGSERSICS
#include "i3_logsersics_definition.h"
#include "i3_image.h"
#include "i3_data_set.h"
#include "i3_options.h"

//i3_flt i3_logsersics_likelihood_old(i3_image * model_image, i3_logsersics_parameter_set * p, i3_data_set * dataset);

//void i3_logsersics_components(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_image * image_dvc, i3_image * image_exp);

void i3_logsersics_image(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image, i3_flt A, int proc_id, int verb);

void i3_logsersics_model_jacobian(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_logsersics_model_jacobian_exact(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_logsersics_model_jacobian_exact_vec(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_flt * jac);

void i3_logsersics_start(i3_logsersics_parameter_set * start, i3_data_set * data_set, i3_options * options);

i3_flt i3_logsersics_likelihood(i3_image * model_image, i3_logsersics_parameter_set * parameters, i3_data_set * dataset);

void i3_logsersics_model_image(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image);
void i3_logsersics_model_image_save_components(i3_logsersics_parameter_set * p, i3_data_set * dataset, i3_image * model_image, bool save_components);

i3_logsersics_parameter_set * i3_logsersics_beermat_params(i3_logsersics_parameter_set * p , i3_options * options);

void i3_logsersics_beermat_mapping(i3_logsersics_parameter_set * input, i3_logsersics_parameter_set * output, i3_options * options);


void i3_logsersics_get_image_info(i3_logsersics_parameter_set * params, i3_data_set * dataset,  i3_flt * image_args, i3_flt * image_info);

#endif
