#include "i3_image.h"
#include "i3_load_data.h"
#include "i3_image_fits.h"



int main(int argc, char * argv[]){
	char * filename = argv[1];
	char * output_filename = argv[2];
	i3_image * image = i3_read_fits_image(filename);
	int nx=image->nx;
	int ny=image->ny;
	printf("i3ut_xy_convention1: read fits image with nx=%d ny=%d\n",nx,ny);
	int status = i3_image_save_fits(image, output_filename);
	printf("i3ut_xy_convention1: saved fits image with status %d\n",status);
	//return status;
}