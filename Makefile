MODELS = sersics logsersics quadratic_test
#star_moffat
MODEL_PREFIX = models
TOOL_PREFIX = tools
DRIVER_PREFIX = drivers
LIB_DIR = lib
TEST_PREFIX = tests
UT_PREFIX = unit_tests
BUILD_DIR = build
BIN_DIR = bin
CC=gcc
F90=gfortran
F77=gfortran
FFLAGS=
USER:=$(shell whoami)
HOST:=$(shell hostname)
VERSION:="$(shell ./bin/get_version)"
PYTHON=python
DOXYGEN=doxygen
DYLIBTOOL=libtool
LIBTOOL=/usr/bin/ar rc
RANLIB=ranlib
WCS=0
SINGLE_PRECISION=0

include Makefile.local
ifneq ($(SINGLE_PRECISION),1)
	CFLAGS += -DI3_FFTW_USE_DOUBLE -DI3_USE_DOUBLE
endif

MODELS_STRING:="$(MODELS)"
CFLAGS += -DI3_LIST_OF_MODELS='$(MODELS_STRING)'

CFLAGS += -Itools -I. -Iextern -DHG_VERSION=$(VERSION) -Iextern/wcstools-libwcs
LINK := -Lextern/levmar -llevmar $(LINK) -Lextern/wcstools-libwcs -lwcs


OS=${IM3SHAPE}
ifeq ($(OS),)
OS=$(shell uname)
endif


ifeq ($(OS),Darwin)
LINK := $(LINK) -framework Accelerate
endif

MODEL_HEADERS = $(MODELS:%=$(MODEL_PREFIX)/i3_%.h)
MODEL_OBJECTS = $(MODELS:%=$(BUILD_DIR)/i3_%.o)
MODEL_DEFINITION_HEADERS = $(MODELS:%=$(MODEL_PREFIX)/i3_%_definition.h)
MODEL_DEFINITION_PY = $(MODELS:%=$(MODEL_PREFIX)/i3_%.py)
MODEL_DEFINITION_DEST = $(MODEL_DEFINITION_PY:%=py3shape/%)

TOOL_HEADERS = i3_global.h i3_image.h i3_load_data.h i3_math.h i3_model.h i3_mcmc.h i3_minimizer.h i3_fisher.h i3_options.h i3_logging.h  i3_model_tools.h i3_great.h i3_utils.h i3_data_set.h i3_pywrapper.h i3_lookup_table.h
HEADER_FILES = $(MODEL_DEFINITION_HEADERS) $(MODEL_HEADERS)  $(TOOL_HEADERS:%=$(TOOL_PREFIX)/%)

FORTRAN_LIBRARIES = $(TOOL_PREFIX)/praxis.o 
F77_LIBRARIES = $(TOOL_PREFIX)/f_lbfgsb.o
LIBRARY_FILENAMES = i3_image.o i3_utils.o i3_psf.o i3_load_data.o i3_options.o i3_model.o i3_mcmc.o i3_minimizer.o i3_fisher.o  i3_gsl_random.o i3_math.o i3_logging.o i3_model_tools.o i3_praxis.o i3_image_fits.o i3_great.o i3_data_set.o i3_analyze.o i3_des_cat.o i3_catalog.o  i3_pywrapper.o i3_lookup_table.o

MODEL_FILES = $(MODEL_OBJECTS)
LIBRARY_NAME = $(LIB_DIR)/libim3.a
DY_LIBRARY_NAME = $(LIB_DIR)/libim3.so

TESTERS = i3_eigv_test i3_lookup_test i3_uxuy_test i3_king_test i3_jacobian_test i3_des_cat_test i3_logging_test i3_local_minima_test i3_beermat_test i3_xy0_max_interp_test i3_unaligned_test i3_hyperbola_test i3_optimizer_test i3_minimizer_test i3_fits_ordering_test i3_pseudo_airy_test i3_airy_test i3_sersic_flux_test i3_fit_test i3_image_test i3_options_test i3_truncate_test i3_rng_test i3_fft_test i3_gaussian_test i3_mcmc_test i3_rotate_test i3_praxis_test i3_convolve_test i3_moffat_test i3_bulge_disc_test i3_downsample_test i3_derivative_test i3_bayesian_estimator_test i3_image_load_save_test i3_great_test i3_likelihood_test i3_xy0_marg_test i3_fits_cube_test i3_padding_test  i3_model_image_exp_disc_test  i3_central_pixel_test i3_ellipticity_sum_test i3_lapack_path_test


ifeq ($(WCS),1)
	TOOL_HEADERS+= i3_wcs.h i3_meds.h
	LIBRARY_FILENAMES+=  i3_wcs.o i3_wcstools.o i3_meds.o
	TESTERS+= i3_wcs_test i3_multiple_exposure_test 
	CFLAGS+=-DUSE_WCS
endif

LIBRARY_FILES = $(LIBRARY_FILENAMES:%=$(BUILD_DIR)/%) 



UNIT_TESTS = i3ut_xy_convention1 i3ut_xy_convention2 i3ut_convolution1 

TESTER_PATHS = $(TESTERS:%=$(BIN_DIR)/%)

NOISE_BIAS_STUDY = im3nbc2 im3nbc_multiple_restarts im3nbc noise_bias_calibration nbc_main nbc_grid_search tom_noise_bias_run tom_noise_bias_run2 tom_compare_ml_be tom_noise_bias_setup tom_model_generator  tom_noise_bias_gaussians    
NOISE_BIAS_STUDY_PATHS =  $(NOISE_BIAS_STUDY:%=$(BIN_DIR)/%)

OPTION_CODE = $(TOOL_PREFIX)/i3_options.c $(TOOL_PREFIX)/i3_options.h
OPTION_BUILDER = $(TOOL_PREFIX)/i3_build_options.py
PARAMETER_FILE = $(INI_PREFIX)/i3_options.ini

MODEL_CODE = $(TOOL_PREFIX)/i3_model.c
MODEL_CODE_TEMPLATE = $(TOOL_PREFIX)/i3_model.template.c
MODEL_HEADER = $(TOOL_PREFIX)/i3_model.h
MODEL_HEADER_TEMPLATE = $(TOOL_PREFIX)/i3_model.template.h
MODEL_BUILDER = $(TOOL_PREFIX)/i3_build_models.py

TEST_PROGRAM_OUTPUT_FILES = i3_image_test_ouput1.txt i3_image_test_ouput2.txt

LEVMAR=extern/levmar/liblevmar.a

LIBWCS=extern/wcstools-libwcs/libwcs.a

all: dirs im3shape dylib copy_modules python/libim3.so 

setup: im3shape dylib 

python/libim3.so: lib/libim3.so
	cp lib/libim3.so py3shape/


lib: dirs $(LIBRARY_NAME) python

doc: Doxyfile $(HEADER_FILES)
	$(DOXYGEN)

dirs:
	mkdir -p lib build bin

python: copy_modules

testers: lib $(TESTERS)

copy_modules: tools/i3_build_options.py tools/i3_build_models.py $(MODEL_DEFINITION_PY)
	cp tools/i3_build_options.py tools/i3_build_models.py $(MODEL_DEFINITION_PY) py3shape/models/



unit_tests: lib $(UNIT_TESTS) im3shape im3generate

im3generate: lib $(DRIVER_PREFIX)/im3generate.c	
	$(CC) $(CFLAGS) -o $(BIN_DIR)/im3generate $(DRIVER_PREFIX)/im3generate.c $(LIBRARY_NAME) $(LINK)

$(BIN_DIR)/im3shape: $(LIBRARY_NAME) $(DRIVER_PREFIX)/im3shape.c	
	$(CC) $(CFLAGS) -o $(BIN_DIR)/im3shape $(DRIVER_PREFIX)/im3shape.c $(LIBRARY_NAME) $(LINK)

im3shape: $(BIN_DIR)/im3shape

noise_bias_g: lib $(DRIVER_PREFIX)/noise_bias_g.c
			$(CC) -o $(BIN_DIR)/noise_bias_g $(DRIVER_PREFIX)/noise_bias_g.c $(CFLAGS)  $(LIBRARY_NAME) $(LINK)

convolution:
	cd extern/convolution && $(MAKE)

$(LEVMAR):
	cd extern/levmar && $(MAKE)

$(LIBWCS):
	cd extern/wcstools-libwcs && $(MAKE)

$(FORTRAN_LIBRARIES): %.o: %.f90
	$(F90) -c $(FFLAGS) $< -o $@ 

$(F77_LIBRARIES): %.o: %.f
	$(F77) -c $(FFLAGS) $< -o $@ 


$(LIBRARY_NAME): $(LEVMAR) $(LIBWCS) $(LIBRARY_FILES) $(FORTRAN_LIBRARIES) $(F77_LIBRARIES) $(MODEL_OBJECTS)
	$(LIBTOOL)  $(LIBRARY_NAME) $(LIBRARY_FILES) $(FORTRAN_LIBRARIES) $(F77_LIBRARIES) $(MODEL_OBJECTS)

$(DY_LIBRARY_NAME): $(LIBRARY_NAME)
	$(CC) $(CFLAGS) -shared -o $(DY_LIBRARY_NAME) $(LIBRARY_FILES) $(F77_LIBRARIES) $(FORTRAN_LIBRARIES) $(MODEL_OBJECTS) -lc $(LINK)
#	$(DYLIBTOOL) -dynamic -o $(DY_LIBRARY_NAME) $(LIBRARY_FILES) $(F77_LIBRARIES) $(FORTRAN_LIBRARIES) $(MODEL_OBJECTS) -lc $(LINK)

dylib: $(DY_LIBRARY_NAME)

$(OPTION_CODE): $(OPTION_BUILDER)
	$(PYTHON) $(OPTION_BUILDER) $(MODELS)


$(MODEL_CODE): $(MODEL_BUILDER) $(MODEL_DEFINITION_PY) $(OPTION_CODE) $(MODEL_CODE_TEMPLATE) $(MODEL_HEADER_TEMPLATE) tools/im3shape.template.py
	$(PYTHON) $(MODEL_BUILDER) --output=$(MODEL_CODE) --header=$(MODEL_HEADER) --dir=$(MODEL_PREFIX) $(MODELS)

$(MODEL_DEFINITION_HEADERS): $(MODEL_CODE) $(MODEL_DEFINITION_PY)

$(LIBRARY_FILES): $(BUILD_DIR)/%.o: $(TOOL_PREFIX)/%.c $(TOOL_PREFIX)/%.h $(MODEL_OBJECTS)
	$(CC) -c $(CFLAGS) $< -o  $@ 

$(MODEL_OBJECTS): $(BUILD_DIR)/%.o: $(MODEL_PREFIX)/%.c $(MODEL_PREFIX)/%.h $(MODEL_CODE)
	$(CC) -c $(CFLAGS) $< -o  $@ 

$(TESTERS): %: $(TEST_PREFIX)/%.c lib
	$(CC)  $< -o $(BIN_DIR)/$@  $(CFLAGS)  $(LIBRARY_NAME) $(LINK)

$(UNIT_TESTS): %: $(UT_PREFIX)/%.c lib
	$(CC)  $< -o $(BIN_DIR)/$@  $(CFLAGS)  $(LIBRARY_NAME) $(LINK)

$(SELF_TEST): %: $(DRIVER_PREFIX)/%.c lib
	$(CC)  $< -o $(BIN_DIR)/$@  $(CFLAGS)  $(LIBRARY_NAME) $(LINK)

$(PARAMETER_FILE): $(OPTION_CODE)

parameters: $(PARAMETER_FILE)

clean:
	rm -f $(LIBRARY_FILES)
	rm -f $(LIBRARY_NAME)
	rm -f $(OPTION_CODE)
	rm -f $(MODEL_CODE) $(MODEL_HEADER)
	rm -f $(PARAMETER_FILE)
	rm -f $(TEST_PROGRAM_OUTPUT_FILES)
	rm -f *.pyc
	rm -f $(DY_LIBRARY_NAME)
	rm -f $(TESTER_PATHS)
	rm -f $(FORTRAN_LIBRARIES)
	rm -f $(F77_LIBRARIES)
	rm -f $(MODEL_PREFIX)/*.pyc
	rm -f $(MODEL_OBJECTS)
	rm -f $(MODEL_DEFINITION_HEADERS)
	rm -f $(NOISE_BIAS_STUDY_PATHS)
	rm -f bin/im3shape bin/great08 bin/great10 bin/sarah_test bin/mcmc_example bin/noise_bias_calibration bin/im3image
	rm -rf bin/*.dSYM
        #Remove debug folders generated on OSX
	rm -rf *.dSYM
	cd extern/convolution && make clean
	cd extern/levmar && $(MAKE) clean
	cd extern/wcstools-libwcs && $(MAKE) clean
	rm -f $(MODEL_DEFINITION_DEST)
	rm -f py3shape/models/i3_build_models.py py3shape/models/i3_build_options.py

.PHONY: all im3shape dylib lib dirs clean parameters levmar convolution lib doc testers copy_modules python


export CC
export CFLAGS
export LINK
export LIBTOOL
export RANLIB
