#include "i3_image.h"
#include "i3_math.h"
#include "i3_gsl_random.h"
#include "i3_model_tools.h"

int main(){
	int n1 = 100;
	int n2 = 100;
	
	i3_gsl_init_rng();
	

	i3_image * image = i3_image_create(n1,n2);
	i3_image_zero(image);
	
	i3_add_real_space_gaussian(image, 20.,0.,0.,1.,40.,30.);

	
	i3_fourier * fft = i3_image_fourier(image);
	i3_image * real_part = i3_fourier_real_part(fft);
	i3_image * imag_part = i3_fourier_imaginary_part(fft);
	
	i3_image_save_text(image,"image.txt");
	i3_image_save_text(real_part,"real.txt");
	i3_image_save_text(imag_part,"imag.txt");
	
	
	
}
