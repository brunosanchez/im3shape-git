close all; clc; clear all;


%%

% put number of models here
nm = 19600; 

% put the truth galaxy id
mid = 12691;%
%noise_std = 0.0230816469;


% load the models
nm = 19600;
npix = 47;
npix2 = npix^2;

fid = fopen('models/models_img_nopsf.dat','r'); M = fread(fid, [npix^2 nm],'double')'; fclose(fid);
model = M(mid,:);
spix = 0.01;    % size of pixel
apix = spix^2;  % area of pixel

% load the noisy image
load obs_img.txt;
noisy = obs_img(:);

%load '~/code/ucl_des_shear/im3shape/ml_study/clear_image.txt';
%load '~/code/ucl_des_shear/im3shape/ml_study/noisy_image.txt';

fid_chi2 = fopen('~/code/ucl_des_shear/im3shape/tests/chi2.dat','r');       
fid_logL1 = fopen('~/code/ucl_des_shear/im3shape/tests/logL1.dat','r');
fid_logL2 = fopen('~/code/ucl_des_shear/im3shape/tests/logL2.dat','r');
fid_prob1 = fopen('~/code/ucl_des_shear/im3shape/tests/prob1.dat','r');
fid_prob2 = fopen('~/code/ucl_des_shear/im3shape/tests/prob2.dat','r');
fid_e   = fopen('~/code/ucl_des_shear/im3shape/ml_study/models/models_e.dat','r');       

chi2 = fread(fid_chi2, nm,'double');
logL1 = fread(fid_logL1, nm,'double');
logL2 = fread(fid_logL2, nm,'double');
prob1 = fread(fid_prob1, nm,'double');
prob2 = fread(fid_prob2, nm,'double');
all_e = fread(fid_e, 2*nm,'double');all_e = reshape(all_e,[2 nm])';all_e = all_e(:,1) + i*all_e(:,2);

% get the full likelihood

%top = -chi2_2 -sum(noisy.^2)/noise_std^2/2 - log(sqrt(sum( model.^2 ))) -nm*log(1/nm);
%logp = -1/2 * chi2_1 - npix2*log(sqrt(2*pi)*noise_std);
%nlogp = logp - max(logp);
%echi2 = exp(nlogp)/sum(exp(nlogp));
% 
% logP = logL - max(logL);
% echi2 = exp(logP)/sum(exp(logP));
% 
% % 
% % chi2_2_full = exp(top);
% % chi2_2_full = chi2_2_full;
% % 
% % % get the mean
% % 
% 

% truth
e_true = all_e(mid)

% ml
[vm,im] = min(chi2);
e_ml = all_e(im)

% bayes
Pe = reshape(prob1,140,140);   % create 2D surface out of 1D vector
e = imag(all_e(1:140));        % get the values for e1 and e2 axes
marg_e1 = sum(Pe,1);           % get marginals for e
marg_e2 = sum(Pe,2)';          
e1 = marg_e1*e;                % get the Bayesian estimator
e2 = marg_e2*e;
e_bayes = e1 + e2*1i    

n = 0;
n=n+1;figure(n); scatter3(real(all_e),imag(all_e),chi2,50,chi2,'filled'); colorbar;
n=n+1;figure(n); scatter3(real(all_e),imag(all_e),logL1,50,logL1,'filled'); colorbar;title('log P(I|e)')
n=n+1;figure(n); scatter3(real(all_e),imag(all_e),logL2,50,logL2,'filled'); colorbar;title('log P(I|e)')
%figure(3); scatter3(real(all_e),imag(all_e),echi2,50,echi2,'filled'); colorbar;title('P(e)')
%figure(4); scatter3(real(all_e),imag(all_e),logP,50,logP,'filled'); colorbar;title('"normalised" log P')
n=n+1;figure(n); scatter3(real(all_e),imag(all_e),prob1,50,prob1,'filled'); colorbar;title(' P(I|e)')
n=n+1;figure(n); scatter3(real(all_e),imag(all_e),prob2,50,prob2,'filled'); colorbar;title(' P(I|e)')
% figure(5); imagesc(reshape(noisy,npix,npix)); colorbar;



%figure; imagesc(clear_image);
%figure; imagesc(noisy_image);
 
% [vmax,gmax] = min(chi2);
% disp(num2str([e1(gmax) e2(gmax)],'ML    = %2.4f %2.4f'));
% disp(num2str([e1(gtru) e2(gtru)],'truth = %2.4f %2.4f'));


% h = scatter3(e1,e2,chi2,50,chi2,'filled','Visible','off');
% saveas(h,'results/likelihood2.fig') 

%num2str([e1 e2 chi2],'%2.4f %2.4f %2.4f')

