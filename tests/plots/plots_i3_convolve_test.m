clc; clear all;



%% test fourier transform
clc;

load image_gal.txt	
load image_pix.txt
load image_psf.txt 
load image_big.txt
load image_obs.txt

load image_pix_c.txt
load image_psf_c.txt 
load image_gal_c.txt
load image_big_c.txt

%% 

n = 0;
n=n+1;figure(n);imagesc(image_gal);colorbar;axis equal; title('image gal')
n=n+1;figure(n);imagesc(image_pix);colorbar;axis equal; title('image pix')
n=n+1;figure(n);imagesc(image_psf);colorbar;axis equal; title('image psf')
n=n+1;figure(n);imagesc(image_big);colorbar;axis equal; title('image big')
n=n+1;figure(n);imagesc(image_obs);colorbar;axis equal; title('image obs')
n=n+1;figure(n);imagesc(image_gal_c);colorbar;axis equal; title('image gal c')
n=n+1;figure(n);imagesc(image_pix_c);colorbar;axis equal; title('image pix c')
n=n+1;figure(n);imagesc(image_psf_c);colorbar;axis equal; title('image psf c')
n=n+1;figure(n);imagesc(image_big_c);colorbar;axis equal; title('image big c')

%% test symetry
clc
g = image_obs;
size(g)
size(g)/2
n=n+1;figure(n);imagesc(g);colorbar; axis equal; title('g')
n=n+1;figure(n);imagesc(g - flipud(g));colorbar;;axis equal;title('g-ud')
n=n+1;figure(n);imagesc(g - fliplr(g));colorbar;;axis equal;title('g-lr')
[vm,im] = max(g(:));
[xmax,ymax] = ind2sub(size(g),im)