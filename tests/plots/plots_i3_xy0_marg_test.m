% ploting for i3_xy0_marg_test
% Plots p(x0,y0|e1,e2,fwhm,model). All conditions are fixed to the
% truth. 
% loads the images used to generate the pdf
% plots the log p (unnormalised) and p (normalised)
% 20110524 TK


%% clean up
clear all; clc

%% check the images

img_dvc = fitsread('image_dvc.fits');
img_exp = fitsread('image_exp.fits');
img_bd = fitsread('image_bd.fits');
img_psf = fitsread('image_psf.fits');
img_abs_pix = fitsread('image_abs_pix.fits');
img_abs_psf = fitsread('image_abs_psf.fits');
img_abs_ker = fitsread('image_abs_ker.fits');
img_obs1 = fitsread('image_obs1.fits');
img_obs2 = fitsread('image_obs2.fits');
img_obs3 = fitsread('image_obs3.fits');
% 
% load disc.txt
% load bulge_disc.txt
% load image_psf.txt
% load abs_psf.txt
% load abs_pix.txt
% load abs_ker.txt
% load image_obs1.txt
% load image_obs2.txt
% load image_obs3.txt

n=0;
n=n+1;figure(n);imagesc(img_dvc);colorbar;
n=n+1;figure(n);imagesc(img_exp);colorbar;
n=n+1;figure(n);imagesc(img_bd);colorbar;
n=n+1;figure(n);imagesc(img_psf);colorbar;
n=n+1;figure(n);imagesc(img_abs_pix);colorbar;
n=n+1;figure(n);imagesc(img_abs_psf);colorbar;
n=n+1;figure(n);imagesc(img_abs_ker);colorbar;
n=n+1;figure(n);imagesc(img_obs1);colorbar;
n=n+1;figure(n);imagesc(img_obs2);colorbar;
n=n+1;figure(n);imagesc(img_obs3);colorbar;



%% load the conditional

% inputs: 
n_sub = 7;
n_add = 1;


n_win = (n_add*2+1);

fid = fopen( 'logL_xy0.dat' ,'r');
logL_xy0 = fread(fid, [n_sub*n_win, n_sub*n_win],'float');
fid = fopen( 'pdf_xy0.dat' ,'r');
pdf_xy0 = fread(fid, [n_sub*n_win, n_sub*n_win],'float');

n=n+1;figure(n);imagesc(logL_xy0); colorbar; 
n=n+1;figure(n);imagesc(pdf_xy0); colorbar; 


%%
