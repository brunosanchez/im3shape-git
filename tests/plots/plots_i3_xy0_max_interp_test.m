close; clear; clc

n_sub=11;
n_pix=39;
n_pad=2;
n_all=n_pix+n_pad;

%%
dirpath='~/code/ucl_des_shear/projects/sersics_tests/';
%filefits='img_exp_h.fits';
filefits='sersics_test_img.fits';
filepath=[dirpath filefits];

img=fitsread(filepath);
imagesc(img);
%img=img(1:(end-1),1:(end-1));

n=0;
n=n+1;figure(n);imagesc(img);colorbar
n=n+1;figure(n);imagesc(fliplr(img)-img);colorbar
n=n+1;figure(n);imagesc(flipud(img)-img);colorbar

[vm,im]=max(img(:));
[mi,mj]=ind2sub(size(img),im)

size(img)

%%

clc 

dirpath='~/code/ucl_des_shear/projects/sersics_tests/';
cd(dirpath)

%% plot the noisy image
clc
filename_obs='img_max_xy0_test_noisy.fits';
obs=fitsread(filename_obs);
obs=obs/sum(obs(:));

n_sub=5;
n_pix=39;
n_pad=4;
n_all=n_pix+n_pad;
n_win = (n_pad+1) * n_sub; 
ns=((n_pad+1)*n_sub);
ns2=ns^2;

filename_dat=['xy_val1_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val1_l=fread(fid,[ns ns],'double');

filename_dat=['xy_val2_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val2_l=fread(fid,[ns ns],'double');

filename_dat=['xy_logL_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
logl_l=fread(fid,[ns ns],'double');


for n=(0:ns2-1)
    
       
           
    filename_mod=sprintf('dsample_cut1_%06d.dat',n);
    mod=fitsread(filename_mod);
    mod=mod./sum(mod(:));
    
    subplot(1,3,1);
    imagesc(mod);colorbar
    str=sprintf('%2.2f %2.2f %2.2e',val1_l(n+1),val2_l(n+1),logl_l(n+1));
    title(str)
    subplot(1,3,2);
    imagesc(obs);colorbar
    str=sprintf('%2.2f %2.2f',21,21);
    title(str)
    subplot(1,3,3);
    imagesc(mod-obs);;colorbar
    
    pause
    
end

%% plot the likelihood in xy0 for n_sub = 7

n_sub=5;
n_pix=39;
n_pad=2;
n_all=n_pix+n_pad;
n_win = (n_pad+1) * n_sub; 

ns=((n_pad+1)*n_sub);
ns2=ns^2;





filename_dat=['xy_val1_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val1_l=fread(fid,[ns ns],'double');

filename_dat=['xy_val2_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val2_l=fread(fid,[ns ns],'double');


[vm im]=max(logl_l(:));
[ix iy]=ind2sub(size(logl_l),im);
fprintf('%f %f\n',[val1_l(im),val2_l(im)])

n=0;
n=n+1;figure(n);imagesc(logl_l);colorbar
%n=n+1;figure(n);imagesc(val1);colorbar
%n=n+1;figure(n);imagesc(val2);colorbar
%n=n+1;figure(n);surf(val1_l,val2_l,logl_l);colorbar;view([0 0])

%% plot the likelihood in xy0 for n_sub = 11

n_sub=81;
n_pix=39;
n_pad=2;
n_all=n_pix+n_pad;
n_win = (n_pad+1) * n_sub; 

ns=((n_pad+1)*n_sub);
ns2=ns^2;

filename_dat=['xy_logL_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
logl_h=fread(fid,[ns ns],'double');


filename_dat=['xy_val1_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val1_h=fread(fid,[ns ns],'double');

filename_dat=['xy_val2_' num2str(n_sub,'%02d') '.dat'];
fid=fopen(filename_dat);
val2_h=fread(fid,[ns ns],'double');

[vm im]=max(logl_h(:));
[ix iy]=ind2sub(size(logl_h),im);
fprintf('%f %f\n',[val1_h(ix),val2_h(ix)]+n_pix/2)

%n=0;
n=n+1;figure(n);imagesc(logl_h);colorbar
%n=n+1;figure(n);imagesc(val1);colorbar
%n=n+1;figure(n);imagesc(val2);colorbar
n=n+1;figure(n);surf(val1_h,val2_h,logl_h);colorbar;view([0 0])


%% interpolate around the max

[vm,im]=max(logl_l(:));
[xm,ym]=ind2sub(size(logl_l),im);

off=1;
x_select = xm-off:xm+off;
y_select = ym-off:ym+off;
max5_logl_l = logl_l(x_select,y_select);
max5_val1_l = val1_l(x_select,y_select);
max5_val2_l = val2_l(x_select,y_select);

X1=max5_val1_l(:) - mean(max5_val1_l(:));
X2=max5_val2_l(:) - mean(max5_val2_l(:));
y =max5_logl_l(:);

size(X1)

eps=1e-5;
X = tk111030_expand_basis( X1, X2, 1);
w=(X'*X + eps*eye(size(X'*X)))\X'*y

%g1=linspace(min(X1),max(X1),100)';
%g2=linspace(min(X2),max(X2),100)';
%[G1,G2]=meshgrid(g1,g2);
%G1=G1(:);
%G2=G2(:);

%% interpolate to the peak of the high res


[vm,im]=max(logl_h(:));
[xm,ym]=ind2sub(size(logl_h),im);

off=25;
x_select = xm-off:xm+off;
y_select = ym-off:ym+off;
max5_logl_h = logl_h(x_select,y_select);
max5_val1_h = val1_h(x_select,y_select);
max5_val2_h = val2_h(x_select,y_select);

G1=max5_val1_h(:) - mean(max5_val1_h(:));
G2=max5_val2_h(:) - mean(max5_val2_h(:));
t =max5_logl_h(:);
P= tk111030_expand_basis( G1, G2, 1);

p=P*w;

%imagesc(reshape(p,10,10));

%%
clf
%scatter3(G1,G2,p,50,p,'filled'); colormap('Hot');
figure(1);scatter3(X1,X2,y,100,y,'filled'); 

figure(2);scatter3(G1,G2,p,100,p,'filled');


di=reshape((p-t)./t,51,51);
figure(3);imagesc(di);colorbar

[vm1,im1] = max(p);
[vm2,im2] = max(t);

[mx1 my1]=ind2sub([51,51],im1);
[mx2 my2]=ind2sub([51,51],im2);
disp(num2str([mx1 my1]));
disp(num2str([mx2 my2]));
disp(num2str([mx1-mx2 my1-my2]./51));


%% plot the noisy image
clc
dirpath='~/code/ucl_des_shear/projects/sersics_tests/';
filename_fits='img_max_xy0_test_noisy.fits';
img=fitsread(filename_fits);
imagesc(img)

%% interpolate the low res to points at high res

logl_hp=interp2(val1_l,val2_l,logl_l,val1_h,val2_h,'spline');
figure(3)
surf(val1_h,val2_h,logl_hp);colorbar;view([0 0])
figure(4)
imagesc((logl_hp-logl_h)./logl_h);colorbar;

[vm1,im1] = max(logl_h(:));
[vm2,im2] = max(logl_hp(:));

[mx1 my1]=ind2sub(size(logl_hp),im1);
[mx2 my2]=ind2sub(size(logl_hp),im2);
disp(num2str([mx1 my1]));
disp(num2str([mx2 my2]));
disp(num2str([mx1-mx2 my1-my2]./51));

%% interpolate around the max using splines


[vm,im]=max(logl_l(:));
[xm,ym]=ind2sub(size(logl_l),im);

off=3;
x_select = xm-off:xm+off;
y_select = ym-off:ym+off;
max5_logl_l = logl_l(x_select,y_select);
max5_val1_l = val1_l(x_select,y_select);
max5_val2_l = val2_l(x_select,y_select);

%max5_logl_l = logl_l;
%max5_val1_l = val1_l;
%max5_val2_l = val2_l;

X1=max5_val1_l - mean(max5_val1_l(:));
X2=max5_val2_l - mean(max5_val2_l(:));
y =max5_logl_l;



[vm,im]=max(logl_h(:));
[xm,ym]=ind2sub(size(logl_h),im);

off=25;
x_select = xm-off:xm+off;
y_select = ym-off:ym+off;
max5_logl_h = logl_h(x_select,y_select);
max5_val1_h = val1_h(x_select,y_select);
max5_val2_h = val2_h(x_select,y_select);

G1=max5_val1_h - mean(max5_val1_h(:));
G2=max5_val2_h - mean(max5_val2_h(:));
t =max5_logl_h;


p = interp2(X1,X2,y,G1,G2,'spline');

clf
%scatter3(G1,G2,p,50,p,'filled'); colormap('Hot');
%figure(1);scatter3(X1,X2,y,100,y,'filled'); 
%figure(2);scatter3(G1,G2,p,100,p,'filled');


di=reshape((p-t)./t,51,51);
figure(3);imagesc(di);colorbar

[vm1,im1] = max(p(:));
[vm2,im2] = max(t(:));

[mx1 my1]=ind2sub([51,51],im1);
[mx2 my2]=ind2sub([51,51],im2);
disp(num2str([mx1 my1]));
disp(num2str([mx2 my2]));
disp(num2str([mx1-mx2 my1-my2]./51));


%% inerpolate all points using polynomials

n_sub=5;
n_pix=39;
n_pad=2;
n_all=n_pix+n_pad;
ns=((n_pad+1)*n_sub);
ns2=ns^2;

X1=val1_l(:) - mean(val1_l(:));
X2=val2_l(:) - mean(val2_l(:));
y =logl_l(:);

eps=1e-5;
%gsl_x = load('gsl_X.dat');
%X = reshape(gsl_x,[ns2,36]);
X = tk111030_expand_basis( X1, X2, 1);

w=(X'*X + eps*eye(size(X'*X)))\X'*y
%w=load('gsl_c.dat');

G1=val1_h(:) - mean(val1_h(:));
G2=val2_h(:) - mean(val2_h(:));

t =logl_h(:);
P = tk111030_expand_basis( G1, G2, 1);

p=P*w;
clf


figure(1);scatter3(X1,X2,y,100,y,'filled'); 
figure(2);scatter3(G1,G2,p,100,p,'filled');


di=reshape((p-t)./t,size(val1_h));
figure(3);imagesc(di);colorbar

[vm1,im1] = max(p);
[vm2,im2] = max(t);

[mx1 my1]=ind2sub(size(val1_h),im1);
[mx2 my2]=ind2sub(size(val1_h),im2);
disp(num2str([mx1 my1],'pred %d %d'));
disp(num2str([G1(im1) G2(im1)] + n_pix/2));
disp(num2str([mx2 my2],'true %d %d'));
disp(num2str([G1(im2) G2(im2)] + n_pix/2));
disp(num2str([mx1-mx2 my1-my2]./51));


%%
clc;

gsl_g1=load('gsl_g1.dat');
gsl_g2=load('gsl_g2.dat');
gsl_x1=load('gsl_x1.dat');
gsl_x2=load('gsl_x2.dat');
gsl_p=load('gsl_p.dat');
gsl_y=load('gsl_y.dat');
gsl_G=load('gsl_G.dat');
gsl_X=load('gsl_X.dat');
gsl_c=load('gsl_c.dat');
    
%scatter3(gsl_g1,gsl_g2,gsl_p)

G = tk111030_expand_basis( gsl_g1, gsl_g2, 1);
p=G*w;

n=0;
n=n+1;figure(n);imagesc(reshape(gsl_X,[36   225])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_G,[36 90000])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_g1,[300 300])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_g2,[300 300])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_p,[300 300])); colorbar
n=n+1;figure(n);imagesc(reshape(G1,[243 243])); colorbar
n=n+1;figure(n);imagesc(reshape(G2,[243 243])); colorbar
n=n+1;figure(n);imagesc(reshape(p,[300 300])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_x1,[15 15])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_x2,[15 15])); colorbar
n=n+1;figure(n);imagesc(reshape(gsl_y,[15 15])); colorbar

%figure(8);imagesc(gsl_c); colorbar

%%

