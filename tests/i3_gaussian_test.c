#include "i3_model_tools.h"
#include "i3_image.h"


int main(){
	int N=48;
	i3_image * image = i3_image_create(N,N);
	i3_add_real_space_gaussian(image, 10.0,0.0,0.1,1.0,N/2.,N/2.);
	i3_image_save_text(image,"gaussian.txt");
	
}
