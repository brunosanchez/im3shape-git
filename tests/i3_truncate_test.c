#include "i3_image.h"
#include "i3_load_data.h"
#include "stdio.h"
#include "stdlib.h"
#include "i3_model_tools.h"
#include "i3_image_fits.h"

int main(){
	int N = 500;
	
	i3_image * image = i3_image_create(N,N);
	i3_image_zero(image);
	i3_flt ab = 100*100;
	i3_flt e1 = 0.0;
	i3_flt e2 = 0.4;
	i3_flt A = 1.0;
	i3_flt x0 = 250.0;
	i3_flt y0 = 150.0;
	i3_flt sersic_n = 1.0;
	i3_flt truncation_factor = 75.0;
	int n_central_upsampling = 5;
	int n_central_pixels_to_upsample = 1;

	i3_add_real_space_sersic_truncated_radius_upsample_central(image, ab,e1,e2,A,x0,y0,sersic_n,truncation_factor,
		n_central_pixels_to_upsample,n_central_upsampling);

	i3_image_save_fits(image,"truncated.fits");
	return 0;
}
