#include "i3_lookup_table.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"

void print_looked_up(i3_sersic_lookup_table * table, i3_flt n, i3_flt u){
	i3_flt logu = i3_log(u);
	i3_flt f = i3_sersic_lookup(table, n, logu);
	printf("%le\t%le\t%le\n",n,u,f);

}
int main(int argc, char * argv[]){
	if (argc==1){
		fprintf(stderr, "Syntax i3_test_lookup table_filename\n");
		exit(1);
	}
	char * filename = argv[1];
	i3_sersic_lookup_table * table = i3_load_lookup_table(filename);

	for (i3_flt u = 0.0001; u<100; u*=1.1){
		print_looked_up(table, 1.0, u);
	}
	printf("\n\n");

	for (i3_flt u = 0.0001; u<100; u*=1.5){
		print_looked_up(table, 4.0, u);
	}
	printf("\n\n");

	for (i3_flt u = 0.0001; u<100; u*=1.5){
		print_looked_up(table, 2.76, u);
	}
	printf("\n\n");


}