#include "i3_mcmc.h"
#include "i3_load_data.h"
#include "i3_math.h"
#include "i3_model.h"
#include "i3_minimizer.h"
#include "i3_fisher.h"
#include "i3_mcmc.h"
#ifdef MPI
#include "mpi.h"
#endif

i3_options * make_options(char * filename){
	i3_options * options = malloc(sizeof(i3_options));
	i3_options_set_defaults(options);
	i3_options_read(options, filename);
	return options;
	
}





int main(int argc, char * argv[]){

	i3_init_rng();

	i3_options * options = make_options(argv[1]);
	i3_model * model = i3_model_create("quadratic_test",options);
	if (!model){fprintf(stderr,"Compiled i3_powell_test without support for quadratic_test model.  Cannot run.\n"); exit(1);}
	i3_parameter_set * initial_parameters = i3_model_option_starts(model,options);
	i3_minimizer * minimizer = i3_minimizer_create(model, options);
	i3_minimizer_result result = i3_minimizer_run_powell(minimizer, initial_parameters,NULL);
	i3_parameter_set * best = i3_minimizer_result_powell(minimizer);
	i3_model_pretty_printf_parameters(model, best);
	fprintf(stdout,"\nresult: %d\n",result);
	return 0;
	
}


