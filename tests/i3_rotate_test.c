#include "i3_image.h"
#include "i3_load_data.h"


int main(){
	i3_image * smiley = i3_read_fits_image("smiley.fits");
	i3_image_save_text(smiley, "smiley.txt");
	/*i3_image * rotated = i3_image_rotate90(smiley);
	#i3_image_save_text(rotated, "rotated.txt");*/
	i3_image * flippedlr = i3_image_create(smiley->nx, smiley->ny);
	i3_image_zero(flippedlr);
	i3_image_fliplr_into(smiley, flippedlr);
	i3_image_save_text(flippedlr, "flippedlr.txt");
}

