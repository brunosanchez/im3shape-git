#!/bin/sh
cd ..
make i3_jacobian_test
cd bin
#valgrind --tool=callgrind i3_jacobian_test ../ini/i3_jacobian_test.ini . 66 66 10 0 3 2.85 0 0 0.9 1 0.99 0.5 0.0 20
i3_jacobian_test ../ini/i3_jacobian_test.ini ../tmp/ 66 66 10 0 3 2.85 0 0 0.9 1 0.99 0.5 0.0 20
cd ../tests
python i3_jacobian_test.py
cd ../tmp
gthumb i3_jacobian_test_2.png &