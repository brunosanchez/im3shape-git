#include "i3_des_cat.h"
#include "i3_logging.h"
#include "i3_load_data.h"
#include "i3_image_fits.h"
#include "stdbool.h"


#define OUTPUT_CAT_NAME "i3_des_cat_test.cat"
#define OUTPUT_IMG_TEMPLATE "i3_des_cat_test_%d.fits"
#define DES_IMG_HDU 2
int main(int argc, char ** argv){
	
	if (argc!=4 && argc!=3){
		fprintf(stderr,"Sytnax: i3_des_cat_test des_cat_filename threshold image_filename\n\timage_filename is optional - add it to cut out the images also and save them.\n\n");
		exit(1);
	}
	i3_set_verbosity(i3_verb_crazy);
	char * filename = argv[1];
	i3_flt threshold = atof(argv[2]);
	char * image_file=NULL;
	if (argc==4) image_file=argv[3];
	
	i3_des_catalog * cat = i3_des_catalog_stars(filename, threshold);
	
	if (cat){
		printf("Found cat with %d entries\n",cat->n);
		printf("First few:\n");
		printf("x       y       size\n");
		for (int i=0;i<5 && i<cat->n;i++){
			printf("%f  %f  %f\n",cat->x[i],cat->y[i],cat->size[i]);
		}
		i3_des_catalog_save(cat,OUTPUT_CAT_NAME);
		if (image_file) save_all_catalog_images(image_filen, cat);
		
	}else{
		fprintf(stderr,"Error loading catalog!\n");
	}

}

void save_all_catalog_images(char * filename, i3_des_catalog * cat)
{
	i3_image * full_image = i3_read_fits_image_extension(image_file,DES_IMG_HDU);
	if (!full_image) return;
	for (int i=0;i<cat->n;i++){
		i3_image * stamp = i3_des_catalog_get_stamp(cat, full_image, i, 24);
		if (!stamp) continue;
		char image_output_filename[256];
		snprintf(image_output_filename,256,OUTPUT_IMG_TEMPLATE,i);
		i3_image_save_fits(stamp,image_output_filename);
	}
}