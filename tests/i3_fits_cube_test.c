#include "i3_load_data.h"
#include "i3_math.h"
int main(int argc, char * argv[]){

	if (argc<2){
		fprintf(stderr,"Usage: i3_fits_cube_test filename.fits\n");
		exit(1);
	}
	char * filename = argv[1];
	int hdu=1;
	int first_image=10000;
	int max_images=0;
	int number_images = i3_fits_cube_number_images(filename,hdu);
	printf("Found %d images\n",number_images);
	i3_image ** images = i3_read_fits_cube(filename, hdu, first_image, max_images, &number_images);
	int brightest_image=-1;
	i3_flt brightest_value=-FLT_MAX;
	for(int i=0;i<number_images;i++){
		i3_flt this_image_max = i3_image_max(images[i]);
		if (this_image_max>brightest_value){
			brightest_value=this_image_max;
			brightest_image=i;
		}
	}
	printf("Read %d images\n",number_images);
	printf("Brightest image is %d, with peak value %f\n",brightest_image,brightest_value);

	i3_image * stack = i3_image_stack(images,number_images);
	char * output_filename = "cube_stack.txt";
	i3_image_save_text(stack,output_filename);
	printf("Average image saved as %s\n",output_filename);
	return 0;
	
	
}