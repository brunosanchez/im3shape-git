#include "i3_model_tools.h"
#include "i3_image.h"
#include "i3_math.h"

int main(){
	i3_init_rng();
	int N=48;
	i3_image * image = i3_image_create(N,N);
	i3_image_add_white_noise(image, 1.0);
	char * filename = "save_test.txt";
	i3_image_save_text(image,filename);
	i3_image * loaded_image = i3_image_load_text(filename);
	if (!loaded_image) I3_FATAL("Failed to reload saved image!",1);
	if (loaded_image->nx != image->nx) I3_FATAL("Failed to reload saved image with right nx!",2);
	if (loaded_image->ny != image->ny) I3_FATAL("Failed to reload saved image with right ny!",3);
	i3_flt tolerance = 1.0e-6;
	for (int i=0;i<image->nx;i++){
		for (int j=0;j<image->ny;j++){
			if (i3_fabs(image->row[i][j]-loaded_image->row[i][j])/image->row[i][j]>tolerance){
				char message[i3_max_string_length];
				snprintf(message,i3_max_string_length,"Error reloading image at pixel (%d,%d): (%f  %f)",i,j,image->row[i][j],loaded_image->row[i][j]);
				I3_FATAL(message,4);
			}
		}
	}
	printf("Reloaded successfully\n");
	return 0;
}
