#include "i3_model.h"
#include "i3_model_tools.h"
#include <time.h>
#ifdef MPI
#include "mpi.h"
#endif

int main(){

// set up 

	i3_init_rng();
	FILE * file;
	i3_flt snr = 20;
	int n_pix = 47;
//	int n_sub = 15;
//	i3_flt cut_off = 0.4;

	int n_grid_e = 141;
	int n_grid_f = 1;
	int n_grid_ee = n_grid_e*n_grid_e;
	int n_grid_eer = n_grid_ee*n_grid_f;

// read models into memory

	printf("reading %d models \n",n_grid_eer);
	i3_image ** models_images = malloc(sizeof(i3_image*)*n_grid_eer);
	i3_flt * grid_eer_e1 = malloc(sizeof(i3_flt)*n_grid_eer);
	i3_flt * grid_eef_e2 = malloc(sizeof(i3_flt)*n_grid_eer);
	i3_flt * grid_eef_fwhm = malloc(sizeof(i3_flt)*n_grid_eer);
	FILE * file_models_images = fopen("ml_study/models/models_0002_images.dat","r");
	FILE * file_models_params = fopen("ml_study/models/models_0002_params.dat","r");
	if(!file_models_images || !file_models_params ){  printf("file error \n"); return I3_PATH_ERROR;  };
	
	for(int i=0;i<n_grid_eer;i++){
		models_images[i] = i3_image_create(n_pix,n_pix);
		fread(models_images[i]->data,sizeof(i3_flt),n_pix*n_pix,file_models_images);
		fread(&grid_eer_e1[i],sizeof(i3_flt),1,file_models_params);
		fread(&grid_eef_e2[i],sizeof(i3_flt),1,file_models_params);
		fread(&grid_eef_fwhm[i],sizeof(i3_flt),1,file_models_params);
	}
	fclose(file_models_params);
	fclose(file_models_images);

// create the grids
	
	i3_flt * grid_ee_e1 = malloc(sizeof(i3_flt)*n_grid_ee);
	i3_flt * grid_ee_e2 = malloc(sizeof(i3_flt)*n_grid_ee);
	
	for(int ge=0;ge<n_grid_ee;ge++){
		grid_ee_e1[ge] = grid_eer_e1[ge*n_grid_f];
		grid_ee_e2[ge] = grid_eef_e2[ge*n_grid_f];
		//printf("%2.3f \t %2.2f \n",grid_ee_e1[ge],grid_ee_e2[ge] );
	}

	i3_flt * grid_e = malloc(sizeof(i3_flt)*n_grid_e);
	for(int ge=0;ge<n_grid_e;ge++){
		grid_e[ge] = grid_ee_e1[ge*n_grid_e];
		//printf("%2.3f \t ", grid_e[ge] );	
	}

	
// choose one of them and calculate chi2

	int mid = 9971;
	i3_image * observed_image = i3_image_copy( models_images[mid] );
	i3_flt e1 = grid_eer_e1[mid];
	i3_flt e2 = grid_eef_e2[mid];
	
	i3_flt noise_std = i3_snr_to_noise_std( snr, observed_image );

	printf("noise_std = %2.10f \n", noise_std);

	i3_image_add_white_noise( observed_image, noise_std );
	i3_image_save_text( observed_image, "ml_study/obs_img.txt" ); 

	i3_flt * logL;
	i3_flt * pdf_marg;
	i3_flt * pdf_full;

	i3_flt * marg_e1 = malloc(sizeof(i3_flt)*n_grid_e);   
	i3_flt * marg_e2 = malloc(sizeof(i3_flt)*n_grid_e);

	i3_flt be_e1 = 0.0;
	i3_flt be_e2 = 0.0;  

// logL 1
	
	logL  = i3_logL_with_models_white_noise_marg_A(models_images, n_grid_eer, observed_image, noise_std );
	pdf_full = i3_pdf_from_logL( logL, n_grid_eer );

	pdf_marg = i3_marginalise_r( pdf_full, n_grid_eer, n_grid_ee );

	printf("getting the marginals \n");

	// get the e marginals
	for(int nge =0; nge<n_grid_e; nge++) {marg_e1[nge] = 0.0; marg_e2[nge] = 0.0;};
	
 	for(int nm=0;nm<n_grid_ee;nm++){
		marg_e1[nm/n_grid_e] += pdf_marg[nm];
		marg_e2[nm%n_grid_e] += pdf_marg[nm];
//		printf("%d \t %+2.2f \t %+2.2f \t %d \t %d \t %2.10e \t \n",nm,grid_eer_e1[nm],grid_eef_e2[nm],nm/n_grid_e,nm%n_grid_e,pdf_marg[nm]);
	}
	
	be_e1 = i3_array_dot( marg_e1, grid_e, n_grid_e );
	be_e2 = i3_array_dot( marg_e2, grid_e, n_grid_e );

	printf("e1_true = \t %2.2f \t e2_true = \t %2.2f \n",e1,e2);
	printf("e1_bayes = \t %2.2f \t e2_bayes = \t %2.2f \n",be_e1, be_e2);
	
	file = fopen("tests/pdfm1.dat","w");
	fwrite(pdf_marg,sizeof(i3_flt),n_grid_ee,file);
 	fclose(file);

	file = fopen("tests/logL1.dat","w");
	fwrite(logL,sizeof(i3_flt),n_grid_eer,file);
 	fclose(file);

	

// logL 2
		
	//logL = i3_logL_with_models_white_noise_fast(models_images, n_grid_eer,  grid_eer_e1,  grid_eer_e1, observed_image, e1, e2, cut_off ,noise_std,1);
	logL = i3_chi2_with_models_white_noise(models_images, n_grid_eer, observed_image, noise_std);
	//logL = i3_logL_with_models_white_noise(models_images, n_grid_eer,  grid_eer_e1,  grid_eer_e1, observed_image, e1, e2, cut_off ,noise_std);
	pdf_full = i3_pdf_from_logL( logL, n_grid_eer );
	pdf_marg = i3_marginalise_r( pdf_full, n_grid_eer, n_grid_ee );

	// get the e marginals	
	for(int nge =0; nge<n_grid_e; nge++) {marg_e1[nge] = 0.0; marg_e2[nge] = 0.0;};

	for(int nm=0;nm<n_grid_ee;nm++){
		marg_e1[nm/n_grid_e] += pdf_marg[nm];
		marg_e2[nm%n_grid_e] += pdf_marg[nm];
	//	printf("%d \t %+2.2f \t %+2.2f \t %d \t %d \t %2.10e \t %2.10e \n",nm,models_e1[nm],models_e2[nm],nm/n_grid,nm%n_grid,Pofe[nm],logL[nm]);
	}
	
	be_e1 = i3_array_dot( marg_e1, grid_e, n_grid_e );
	be_e2 = i3_array_dot( marg_e2, grid_e, n_grid_e );

	file = fopen("tests/pdfm2.dat","w");
	fwrite(pdf_marg,sizeof(i3_flt),n_grid_eer,file);
 	fclose(file);

	file = fopen("tests/logL2.dat","w");
	fwrite(logL,sizeof(i3_flt),n_grid_eer,file);
 	fclose(file);

	printf("e1_true = \t %2.2f \t e2_true = \t %2.2f \n",e1,e2);
	printf("e1_bayes = \t %2.2f \t e2_bayes = \t %2.2f \n",be_e1, be_e2);
	

	return 0;
}
