import galsim
import os
import sys
IM3SHAPE_PATH = os.path.join('..','python')
sys.path.append(IM3SHAPE_PATH)
import im3shape
print 'loaded im3shape'

n_pix = 39
n_subpix = 9
n_pad = 4
n_pix_psf = (n_pix+n_pad)*n_subpix

pixel_scale_gal = 1.
pixel_scale_psf = pixel_scale_gal/n_subpix

g1=g2=0.2

# exp = galsim.Exponential(half_light_radius = 2.)
# exp.applyShear(g1=g1,g2=g2)

exp = galsim.Sersic(half_light_radius = 4.,n=4)
exp.applyShear(g1=0.2,g2=0.2)


psf_fwhm = 2.85
psf_beta = 3.
psf = galsim.Moffat(fwhm=psf_fwhm,beta=psf_beta)
pix = galsim.Pixel(pixel_scale_gal)

final = galsim.Convolve([exp,psf,pix])

img = galsim.ImageD(n_pix,n_pix)

img_psf = galsim.ImageD(n_pix_psf,n_pix_psf)

final.setFlux(1.)

final.draw(img,dx=pixel_scale_gal)
psf.draw(img_psf,dx=pixel_scale_psf)


random_seed = 1534233
rng = galsim.BaseDeviate(random_seed)
# noise = galsim.PoissonNoise(rng, sky_level=sky_level_pixel)
noise = galsim.GaussianNoise(rng, sigma=0.002)
img.addNoise(noise)

import pylab
pylab.imshow(img.array,interpolation="nearest")
# pylab.colorbar(); pylab.show(); 
pylab.imshow(img_psf.array,interpolation="nearest")
# pylab.show()


i3o = im3shape.I3_options()
i3o.read_ini_file('../example/example.ini')
i3o.stamp_size = n_pix
i3o.upsampling = n_subpix
i3o.padding = n_pad
i3o.noise_sigma = 1
i3o.minimizer_verbosity = 0
i3o.levmar_eps2 = 1e-200
i3o.levmar_eps4 = -1
i3o.levmar_eps5 = 1e-6
i3o.n_central_pixel_upsampling = 7
i3o.n_central_pixels_to_upsample = 9
i3o.verbosity = -1

i3_galaxy = im3shape.I3_image(n_pix, n_pix)
i3_galaxy.from_array(img.array)

i3_psf = im3shape.I3_image(n_pix_psf, n_pix_psf)
i3_psf.from_array(img_psf.array)

print 'running the old procedure'

import time
t1=  time.time()
i3_result, i3_best_fit = im3shape.i3_analyze(i3_galaxy, i3_psf, i3o, ID=0)
print 'time for the old procedure: ' + str(time.time() - t1)

print "i3_result.likelihood\t" + str(i3_result.likelihood)
print "i3_result.sersic_parameter_x0\t" + str(i3_result.sersic_parameter_x0)
print "i3_result.sersic_parameter_y0\t" + str(i3_result.sersic_parameter_y0)
print "i3_result.sersic_disc_flux\t" + str(i3_result.sersic_disc_flux)
print "i3_result.sersic_bulge_flux\t" + str(i3_result.sersic_bulge_flux)
print "i3_result.sersic_parameter_bulge_index\t" + str(i3_result.sersic_parameter_bulge_index)
print "i3_result.sersic_parameter_disc_index\t" + str(i3_result.sersic_parameter_disc_index)
print "i3_result.sersic_parameter_e1\t" + str(i3_result.sersic_parameter_e1)
print "i3_result.sersic_parameter_e2\t" + str(i3_result.sersic_parameter_e2)
print "i3_result.sersic_parameter_radius\t" + str(i3_result.sersic_parameter_radius)

print 'running the new procedure'

im3shape.i3_analyse_with_prerun(i3_galaxy, i3_psf, i3o,verb=True)



