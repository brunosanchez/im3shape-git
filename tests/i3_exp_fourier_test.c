#include "i3_model_tools.h"


int main(){
	i3_fftw_load_wisdom();
	atexit(i3_fftw_save_wisdom);
	
	{
		int n = 100;
		i3_image * image = i3_image_create(n,n);	
		// i3_image * image2 = i3_image_create(n,n);	
		i3_fourier * fourier = i3_fourier_create(n,n);
	
		i3_flt ab = 20.0;
		i3_flt e1 = 0.0;
		i3_flt e2 = 0.0;
		i3_flt A = 1;
		i3_flt x0 = 0.0;
		i3_flt y0 = 0.0;
		i3_flt sersic_n = 1.0;
	
		i3_image_zero(image);
		i3_add_real_space_sersic(image, ab,e1,e2,A,x0,y0, sersic_n);
		i3_image_save_text(image,"exp_direct1.txt");
	
		for(int i=0;i<fourier->n_f;i++) fourier->data[i] = 0.0;
		i3_add_fourier_space_exponential(ab,e1,e2,A,x0,y0, fourier);
		FILE * f = fopen("x1","w");
		for(int i=0;i<fourier->n_f;i++) fprintf(f,"%lf  %lf\n",creal(fourier->data[i]),cimag(fourier->data[i]));
		fclose(f);
		i3_image * image2 = i3_fourier_image(fourier);

		// i3_image_save_text(image,"exp.txt");
		i3_image_save_text(image2,"exp1.txt");

		i3_image_destroy(image);
		i3_image_destroy(image2);
		i3_fourier_destroy(fourier);
	}
	{
		int n = 100;
		i3_image * image = i3_image_create(n,n);	
		i3_fourier * fourier = i3_fourier_create(n,n);
	
		i3_flt ab = 2.0;
		i3_flt e1 = 0.0;
		i3_flt e2 = 0.0;
		i3_flt A = 1;
		i3_flt x0 = 0.0;
		i3_flt y0 = 0.0;
		i3_flt sersic_n = 1.0;
	
		i3_image_zero(image);
		i3_add_real_space_sersic(image, ab,e1,e2,A,x0,y0, sersic_n);
		i3_image_save_text(image,"exp_direct2.txt");
	
	
		for(int i=0;i<fourier->n_f;i++) fourier->data[i] = 0.0;
		i3_add_fourier_space_exponential(ab,e1,e2,A,x0,y0, fourier);
		FILE * f = fopen("x2","w");
		for(int i=0;i<fourier->n_f;i++) fprintf(f,"%lf  %lf\n",creal(fourier->data[i]),cimag(fourier->data[i]));
		fclose(f);
		i3_image * image2 = i3_fourier_image(fourier);

		// i3_image_save_text(image,"exp.txt");
		i3_image_save_text(image2,"exp2.txt");

		i3_image_destroy(image);
		i3_image_destroy(image2);
		i3_fourier_destroy(fourier);
	}


}

