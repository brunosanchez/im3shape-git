# This is a postburner to running im3shape.py with 
#   i3_result, i3_best_fit = i3_analyze_with_permutations(i3_galaxy, i3_moffat_psf, i3o, ID=identifier, x=xpos, y=ypos)
# instead of the usual i3_analyze function
# It reads in the multi-line im3shape output file and processes it into
# two files:
# A pseudo-im3shape output file for the image(s) that were analysed


import pylab as pl
import numpy as np
import pyfits as pf
import subprocess as sp
import ipdb
import re


# Read im3shape output for each of the image flip/rotate permutations
def i3_unpermutations(i3_params_file, i3_out_file, i3_out_file_gold, i3_out_file_stddev):

  # Create space to store the results
  nperms=8
  i3_out_line_length=35
  #e1_values=np.zeros(nperms)
  #e2_values=np.zeros(nperms)
  output_values=np.zeros([nperms,i3_out_line_length])

  
  # Read in 8 lines of the output file and process them one batch at a time
  data=loadtxt(i3_out_file)
  # could do .T
  
  iperm=0
  f=open(i3_out_file, 'r')
  for line in f:
    print line,
    iscomment = re.match('^#', line)
    if not iscomment:
      numbers=line.split()
      output_values[iperm,:] = numbers
      iperm=iperm+1
    if iperm==8:
      # Process this object now
      print "hello"

      # Unpermute the x, y, e1, e2 values
      x_values = output_values[:,4]
      y_values = output_values[:,5]
      e1_values = output_values[:,6]
      e2_values = output_values[:,7]
      # !!! still need to unpermute the x, y values !!! cheat for now!!!
      #x_values_unpermute = x_values[0,0,2,3,2,3,7,7]
      x_values_unpermute = x_values
      y_values_unpermute = y_values
      ipdb.set_trace()
      e1_values *= np.array([1, -1, -1, 1, 1, -1, -1, 1])
      e2_values *= np.array([1, -1, -1, 1, 1, -1, -1, 1])

  f.close()

  ipdb.set_trace()


  # Rotate the fits image by permutation type i
  #for permutation_id in np.arange(nperms):
  #  i3_image_permute(i3_fits_file, permutation_id, i3_fits_file_tmp)


  # De-permute the e1, e2 values 

  # tidy up
  mean_e1=e1_values.mean()
  mean_e2=e2_values.mean()
  std_e1=e1_values.std()
  std_e2=e2_values.std()
  output_values=numbers


  ipdb.set_trace()

  return mean_e1
  return mean_e2
  return std_e1
  return std_e2
  return output_values


# A wrapper to generate a galaxy image using im3shape
def i3_generate_py(i3_exe,i3_params_file,i3_fits_file,i3_cat_file_tmp,i3_psf_file_tmp,stamp_size,**params):
  import subprocess as sp

  args = ["%s=%f"%(param,value) for param,value in params.items()]
  i3_usage="--generate"
  sp.call([i3_exe, i3_usage, i3_params_file, i3_fits_file,] + args) 

  # Make a corresponding PSF file
  f=open(i3_psf_file_tmp, 'w')
  f.write("# beta fwhm e1 e2\n")
  f.write("%f %f %f %f\n"%(params['psf_beta'],params['psf_fwhm'],params['psf_e1'],params['psf_e2']))
  f.write("%f %f %f %f\n"%(params['psf_beta'],params['psf_fwhm'],params['psf_e1'],params['psf_e2']))
  f.close()

  # And make a corresponding catalogue file
  f=open(i3_cat_file_tmp, 'w')
  f.write("#ID  x  y\n")
  f.write("%i %f %f\n"%(0.0,stamp_size/2.0,stamp_size/2.0))
  f.write("%i %f %f\n"%(1.0,stamp_size/2.0,stamp_size/2.0))
  f.close()



if __name__ == "__main__":

  # Set interesting parameters for this run
  tag="_20130416"
  psf_fwhm=2.85
  psf_e1=0 # IT DOESN"T WORK YET IF PSF ISN"T CIRCULAR!
  psf_e2=0
  e1=0.2
  e2=0.1
  x0=17.5
  y0=17.5
  sigma=0.1

  # Set boring parameters for this run
  i3_exe_generate="/Users/sarah/mercurial/im3shape/bin/im3shape"
  i3_exe_analyze="python /Users/sarah/mercurial/im3shape/python/im3shape.py"
  wkdir='/Users/sarah/scratch/gravlens/noise_rotation/20130228/'
  i3_params_file=wkdir+"params_noiserot"+tag+".ini"
  i3_fits_file=wkdir+"true_galaxy"+tag+".fits"
  i3_fits_file_tmp=wkdir+"temp_galaxy"+tag+".fits"
  i3_cat_file_tmp=wkdir+"catalog_noiserot"+tag+".txt"
  i3_psf_file_tmp=wkdir+"psf"+tag+".txt"
  i3_out_file="i3_out"+tag+".txt"
  i3_out_file_gold="i3_out_gold"+tag+".txt"
  i3_out_file_stddev=wkdir+"i3_out_stddev"+tag+".txt"
  psf_beta=3
  stamp_size=37 # Must match the value in the params file

  # Generate a galaxy image using im3shape
  i3_generate_py(i3_exe_generate,i3_params_file,i3_fits_file, i3_cat_file_tmp,i3_psf_file_tmp, stamp_size, 
    e1=e1, e2=e2, x0=x0, y0=y0, psf_beta=psf_beta, psf_fwhm=psf_fwhm, psf_e1=psf_e1, psf_e2=psf_e2)

  # Run the python wrapper with perumations switched on
  # (currently this is hard-wired!)
  string = [i3_exe_analyze+' '+i3_params_file+' '+i3_fits_file_tmp+' '+
    i3_cat_file_tmp+' '+i3_psf_file_tmp+' '+i3_out_file+' 0 0']
  print string

  # ipdb.set_trace()
  # sp.call(string)
  # ipdb.set_trace()

  # Run the un-flipping function to find best and stddev of the im3shape outputs
  i3_stddev = i3_unpermutations(i3_params_file, i3_out_file, i3_out_file_gold, i3_out_file_stddev)
  # , i3_best




  

