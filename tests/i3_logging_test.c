#include "i3_logging.h"

int main(){
	i3_print(i3_verb_quiet,"This should print the number %f",4.34);
	i3_print(i3_verb_standard,"This should print the number %d",876);
	i3_print(i3_verb_noisy,"This should NOT print the number %d",666);
	i3_set_verbosity(i3_verb_debug);
	i3_print(i3_verb_noisy,"This should print a string: %s","STRING");
	i3_print(i3_verb_debug,"This should should print the number %e",4.23453e-4);
	i3_print(i3_verb_extreme,"This should NOT print the number %e",66.666-45);
	printf("Should have printed four lines plus this one\n");
}