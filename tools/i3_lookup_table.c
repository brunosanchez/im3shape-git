#include "i3_lookup_table.h"
#include "i3_load_data.h"
#include "math.h"
#include "fitsio.h"

inline static i3_flt lookup_table_n_value(i3_sersic_lookup_table * table, int i)
{
	return table->min_n + table->delta_n*i;
}

inline static int lookup_table_logu_value(i3_sersic_lookup_table * table, int i)
{
	return table->min_logu + table->delta_logu*i;
}

inline static int lookup_table_n_index(i3_sersic_lookup_table * table, i3_flt n)
{
	// printf("%le  %le  %le\n",n,(n-table->min_n)/table->delta_n,floor((n-table->min_n)/table->delta_n));
	return (int)floor((n-table->min_n)/table->delta_n);
}

inline static int lookup_table_logu_index(i3_sersic_lookup_table * table, i3_flt logu)
{
	return (int)floor((logu-table->min_logu)/table->delta_logu);
}

static inline i3_flt lookup_single_row(i3_sersic_lookup_table * table, i3_flt logu, i3_flt * row)
{
	int j = lookup_table_logu_index(table,logu);
	i3_flt u1 = table->min_logu+table->delta_logu*j;
	i3_flt r_u = (logu-u1)/table->delta_logu;	
	i3_flt f = row[j]*(1-r_u)+row[j+1]*(r_u);
	return f;

}


i3_sersic_lookup_table * i3_load_lookup_table(char * filename)
{
	i3_sersic_lookup_table * table = malloc(sizeof(i3_sersic_lookup_table));

	// Load the table as an image using the utility function.
	i3_image * lookup_table_image = i3_read_fits_image(filename);

	// Then steal stuff from the image before freeing it (not destroying)
	table->raw_data = lookup_table_image->data;
	table->table = lookup_table_image->row;
	// k is in log direction because it is more frequent that we use 
	// a fixed n value
	table->n_logu = lookup_table_image->nx;
	table->n_n = lookup_table_image->ny;


	// Open the FITS file again to read the header keywords
	fitsfile * input_file;
	int status = 0;
	int hdu = 1;
	int hdutype = 0;
	int datatype = i3_get_fitsio_flt_dtype();
	double min_n, delta_n;
	double min_logu, delta_logu;
	char comment[72];
	fits_open_file(&input_file, filename, READONLY, &status);
	fits_movabs_hdu(input_file, hdu, &hdutype, &status);
	fits_read_key(input_file, datatype, "MIN_N", &min_n, comment, &status);
	fits_read_key(input_file, datatype, "D_N", &delta_n, comment, &status);
	fits_read_key(input_file, datatype, "MIN_LOGU", &min_logu, comment, &status);
	fits_read_key(input_file, datatype, "D_LOGU", &delta_logu, comment, &status);

	// If any error return NULL
	if (status) {
		i3_image_destroy(lookup_table_image);
		free(table);
		I3_WARNING("Failed to load lookup table");
		fits_report_error(stderr,status);
		return NULL;
	}	

	// Fill in the other table convenience parameters
	table->min_n = min_n;
	table->delta_n = delta_n;
	table->min_logu = min_logu;
	table->delta_logu = delta_logu;
	table->max_n = table->min_n + table->delta_n*(table->n_n-1);
	table->max_logu = table->min_logu + table->delta_logu*(table->n_logu-1);

	// Special values likely to be looked up often - save the locations
	// of these values
	table->i_exponential=-1;
	table->i_devaucouleurs=-1;
	for (int i=0; i<table->n_n; i++){
		if (i3_fabs(lookup_table_n_value(table,i)-1.0)<1e-6) table->i_exponential=i;
		if (i3_fabs(lookup_table_n_value(table,i)-4.0)<1e-6) table->i_devaucouleurs=i;
	}

	// Non-fatal warning if the fixed values are not found
	if (table->i_exponential==-1) 
		I3_WARNING("The sersics lookup table did not have a value near n=1 exponential");
	if (table->i_devaucouleurs==-1) 
		I3_WARNING("The sersics lookup table did not have a value near n=4 devaucouleurs");

	// We do not destroy the image, just free it.
	// Because we want to keep the data it allocated in the table.
	free(lookup_table_image);

	return table;
}


i3_flt i3_sersic_lookup(i3_sersic_lookup_table * table, i3_flt n, i3_flt logu)
{
	// Check if we can specialize to the faster functions where we already have the other bits
	if (n==1.0 && (table->i_exponential!=-1)) return  i3_exponential_lookup(table,logu);
	if (n==4.0 && (table->i_devaucouleurs!=-1)) return  i3_devaucouleurs_lookup(table,logu);
	if (n<table->min_n || n>table->max_n) {
		I3_FATAL("Tried to look up a sersic index outside the lookup table range",1);
	}
	if (logu<table->min_logu) return 1.0;
	if (logu>table->max_logu){
		I3_FATAL("Tried to look up a sersic index outside the lookup table range in log u",1);
	}

	// Find the table locations
	int i = lookup_table_n_index(table,n);
	int j = lookup_table_logu_index(table,logu);

	// Check for out of range
	// j<0 cannot happen as checked above
	if (i<0 || i>=table->n_n-1) I3_FATAL("Tried to look up a sersic index outside the lookup table n range",1);
	if (j>=table->n_logu-1) I3_FATAL("Tried to look up a sersic index outside the lookup table u range",1);

	// Pull out the row we need, and how far between the two rows we are using
	i3_flt * row1 = table->table[i];
	i3_flt n1 = table->min_n+table->delta_n*i;
	// printf("n1 = %le\n",n1);
	// Potential short cut - if our n value is the same as a row n then we 
	// do not need to do bilinear interpolation, only single
	if (i3_fabs(n1-n)<1e-6) return lookup_single_row(table, logu, row1);
	// printf("n1 = %le\n",n1);

	// Otherwise do the full bilinear interp
	i3_flt * row2 = table->table[i+1];
	i3_flt r_n = (n-n1)/table->delta_n;

	i3_flt u1 = table->min_logu+table->delta_logu*j;
	i3_flt r_u = (logu-u1)/table->delta_logu;
	// printf("vertices:\n");
	// printf("n=%le  log(u)=%le   f=%le\n", n1, u1, row1[j]);
	// printf("n=%le  log(u)=%le   f=%le\n", n1+table->delta_n, u1, row2[j]);
	// printf("n=%le  log(u)=%le   f=%le\n", n1, u1+table->delta_logu, row1[j+1]);
	// printf("n=%le  log(u)=%le   f=%le\n", n1+table->delta_n, u1+table->delta_logu, row2[j+1]);
	// printf("r_u=%le   r_n=%le\n", r_u, r_n);
	i3_flt f1 = row1[j]*(1-r_u)+row1[j+1]*(r_u);
	i3_flt f2 = row2[j]*(1-r_u)+row2[j+1]*(r_u);

	i3_flt f = f1*(1-r_n) + f2*(r_n);
	return f;


}



i3_flt i3_exponential_lookup(i3_sersic_lookup_table * table, i3_flt logu)
{
	if (table->i_exponential==-1) 
		return i3_sersic_lookup(table, 1.0, logu);
	i3_flt * row = table->table[table->i_exponential];
	return lookup_single_row(table, logu, row);

}
i3_flt i3_devaucouleurs_lookup(i3_sersic_lookup_table * table, i3_flt logu)
{
	if (table->i_exponential==-1) 
		return i3_sersic_lookup(table, 4.0, logu);
	i3_flt * row = table->table[table->i_devaucouleurs];
	return lookup_single_row(table, logu, row);
}