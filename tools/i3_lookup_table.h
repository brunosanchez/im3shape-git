#include "i3_global.h"

typedef struct i3_sersic_lookup_table{
	int n_n;
	int n_logu;
	i3_flt delta_n;
	i3_flt delta_logu;
	i3_flt ** table;
	i3_flt * raw_data;
	i3_flt min_n;
	i3_flt min_logu;
	i3_flt max_n;
	i3_flt max_logu;
	int i_exponential;
	int i_devaucouleurs;
} i3_sersic_lookup_table;


i3_sersic_lookup_table * i3_load_lookup_table(char * filename);
i3_flt i3_sersic_lookup(i3_sersic_lookup_table * table, i3_flt n, i3_flt logu);
i3_flt i3_exponential_lookup(i3_sersic_lookup_table * table, i3_flt logu);
i3_flt i3_devaucouleurs_lookup(i3_sersic_lookup_table * table, i3_flt logu);
