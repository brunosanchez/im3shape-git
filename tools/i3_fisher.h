#include "i3_model.h"
#include "i3_data_set.h"

#define FISHER_MIN_JUMP_SIZE 0.2
#define FISHER_MAX_JUMP_SIZE 2

/**
 * NOT CURRENTLY FUNCTIONING
**/
i3_parameter_set * i3_fisher_compute_diagonal(i3_model * model, i3_data_set * data_set, i3_parameter_set * center_params, i3_parameter_set * guess_params);

