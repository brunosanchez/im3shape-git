/*
i3_load_data.c
Created by JAZ.  27/10/10.

Routines for loading images from FITS files. 

A quick note on FITS images:  Every FITS file consists of one or more Header-Data Units (HDUs),
each of which consists of a text header section and (possibly) a binary or text data section.

The first HDU in a file is called the primary, and if it has data then that data must be in the
form of an image - tables are not allowed.  Subsequent HDUs can contain table data in binary or 
ascii format, so we must check whether they are the right type.

For more info on the CFITSIO library see:
http://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/cfitsio.html
*/
#include "i3_load_data.h"
#include "fitsio.h"

/*
	i3_read_fits_image
	Read the image from the primary (0) HDU in the named fits file.
	This is simply a convenience function to call i3_read_fits_image_extension
	with the first HDU in the file.
*/

int i3_fits_cube_number_images(char * filename, int hdu){
	int status=0;
	int hdutype;
	long image_size[3];
	int image_axes;
	fitsfile * input_file;
	
	/* Open the FITS image.  We do not check the status immediately as FITS routines
	   check the input value of status and return immediately if non-zero.
	
		However this routine exits we make sure to close the file before we leave.
	*/
	fits_open_file(&input_file, filename, READONLY, &status);
	
	/* Move to the relevant HDU and check the image type and dimension.
		For now we assume that the image is 2D but this might change depending on how 
		files are structured.
	 */
	fits_movabs_hdu(input_file, hdu, &hdutype, &status);
	fits_get_img_dim(input_file, &image_axes, &status);

	if (status){
		fprintf(stderr,"Error opening fits file %s HDU %d:",filename,hdu);
		fits_report_error(stderr,status);
		fits_close_file(input_file,&status);
		return 0;
	}

	if (image_axes!=3){
		fprintf(stderr,"Image in FITS cube file %s HDU %d has bad dimensionality: %d (expecting 3)\n",filename,hdu,image_axes);
		fits_close_file(input_file,&status);
		return 0;
	}
	
	/* Get the image size, assign memory and read the image in. */
	fits_get_img_size(input_file, image_axes, image_size, &status);
	fits_close_file(input_file,&status);
	
	return image_size[2];
}

i3_image ** i3_read_fits_cube(char * filename, int hdu, int first_image, int max_images, int * number_images){
	int status=0;
	int hdutype;
	long image_size[3];
	int image_axes;
	i3_image ** images;
	fitsfile * input_file;
	// long image_npix;
	int any_null;
	float null_value=0.0;
	int i3_fitsio_type;
	int total_number_images;

	/* Open the FITS image.  We do not check the status immediately as FITS routines
	   check the input value of status and return immediately if non-zero.
	
		However this routine exits we make sure to close the file before we leave.
	*/
	fits_open_file(&input_file, filename, READONLY, &status);
	
	/* Move to the relevant HDU and check the image type and dimension.
		For now we assume that the image is 2D but this might change depending on how 
		files are structured.
	 */
	fits_movabs_hdu(input_file, hdu, &hdutype, &status);
	fits_get_img_dim(input_file, &image_axes, &status);

	if (status){
		fprintf(stderr,"Error opening fits file %s HDU %d:",filename,hdu);
		fits_report_error(stderr,status);
		fits_close_file(input_file,&status);
		*number_images=0;
		return NULL;
	}

	if (image_axes!=3){
		fprintf(stderr,"Image in FITS cube file %s HDU %d has bad dimensionality: %d (expecting 3)\n",filename,hdu,image_axes);
		fits_close_file(input_file,&status);
		*number_images=0;
		return NULL;
	}
	
	/* Get the image size, assign memory and read the image in. */
	fits_get_img_size(input_file, image_axes, image_size, &status);
	
	total_number_images = image_size[2];
	
	if (first_image>=total_number_images){
		fprintf(stderr,"You asked for images starting at number %d from file %s, which only has %d images in it.\n",first_image,filename,total_number_images);
		fits_close_file(input_file,&status);
		*number_images=0;
		return NULL;
		
	}
	
	/* 
	Decide how many images to read/
	*/
	int images_read;
	if (max_images==0){
		images_read = total_number_images-first_image;
	}
	else{
		images_read = (total_number_images-first_image<=max_images) ? (total_number_images-first_image) : max_images;
	}
	
	
	images = (i3_image**)malloc(sizeof(i3_image*)*images_read);
	i3_fitsio_type=i3_get_fitsio_flt_dtype();

	// FITS pixel numbering starts at 1.
	long first_pixel[3] = {1,1,0};
	for (int i=0;i<images_read;i++){
		images[i]=i3_image_create(image_size[0],image_size[1]);
		first_pixel[2]=first_image+i+1;
		fits_read_pix(input_file, i3_fitsio_type, first_pixel, images[i]->n, &null_value, images[i]->data, &any_null, &status)	;
		if (status){
			printf("Status bad (%d) for image %d\n",status,i);
			fits_report_error(stderr,status);
			fits_close_file(input_file,&status);
			for (int j=0;j<=i;j++){
				i3_image_destroy(images[j]);
			}
			*number_images=0;
			free(images);
			return NULL;
		}
	}
	fits_close_file(input_file, &status);
	*number_images = images_read;
	return images;

	
}

i3_image * i3_read_fits_image(char * filename){
	return i3_read_fits_image_extension(filename,1);
}

/*
	i3_read_fits_image_extension
	Load the image from the numbered HDU.
*/	
i3_image * i3_read_fits_image_extension(char * filename, const int hdu){
	int status=0;
	int hdutype;
	long image_size[2];
	int image_axes;
	i3_image * image;
	fitsfile * input_file;
	long image_npix;
	int any_null=0;
	float null_value=0.0;
	int i3_fitsio_type;
	
	/* Open the FITS image.  We do not check the status immediately as FITS routines
	   check the input value of status and return immediately if non-zero.
	
		However this routine exits we make sure to close the file before we leave.
	*/
	fits_open_file(&input_file, filename, READONLY, &status);
	
	/* Move to the relevant HDU and check the image type and dimension.
		For now we assume that the image is 2D but this might change depending on how 
		files are structured.
	 */
	fits_movabs_hdu(input_file, hdu, &hdutype, &status);
	fits_get_img_dim(input_file, &image_axes, &status);

	if (status){
		fprintf(stderr,"Error opening fits file %s HDU %d:",filename,hdu);
		fits_report_error(stderr,status);
		fits_close_file(input_file,&status);
		return NULL;
	}

	/*
	Check that the image is 2D.  If not then return NULL.
	*/
	if (image_axes!=2){
		fprintf(stderr,"Image in FITS file %s HDU %d has bad dimensionality: %d (expecting 2)\n",filename,hdu,image_axes);
		fits_close_file(input_file,&status);
		return NULL;
	}
	
	/* Get the image size, assign memory and read the image in. */
	fits_get_img_size(input_file, image_axes, image_size, &status);

	/* We are about to start allocating memory now so this is a good time to our status so far.*/
	if (status){
		fprintf(stderr,"Error opening fits file %s HDU %d:",filename,hdu);
		fits_report_error(stderr,status);
		fits_close_file(input_file,&status);
		return NULL;
	}
	
	/* Check the image is a sensible size*/
	if (image_size[0]<=0 || image_size[1]<=0){
		fprintf(stderr,"Image in FITS file %s HDU %d has bad image size: %ldx%ld\n",filename,hdu,image_size[0],image_size[1]);
		fits_close_file(input_file,&status);
		return NULL;
	}
	
	image = i3_image_create(image_size[0],image_size[1]);
	
	if (!image){
		/* The most likely error here is a memory error.*/
		fprintf(stderr,"Unable to allocate image for  FITS file %s HDU %d (size: %ldx%ld)\n",filename,hdu,image_size[0],image_size[1]);
		fits_close_file(input_file,&status);
		return NULL;
	}
	i3_image_zero(image);
	image_npix = image->n;
	i3_fitsio_type=i3_get_fitsio_flt_dtype();
	fits_read_img(input_file,i3_fitsio_type, 1, image_npix, &null_value, image->data, &any_null, &status);

	if (status){
		i3_image_destroy(image);
		fprintf(stderr,"Unable to read image for  FITS file %s HDU %d (size: %ldx%ld)\n",filename,hdu,image_size[0],image_size[1]);
		fits_report_error(stderr,status);
		fits_close_file(input_file,&status);
		return NULL;
	}
	
	fits_close_file(input_file,&status);
	return image;
	
	
}

/*
Get the fitsio data type corresponding to the definition of i3_flt.
*/

int i3_get_fitsio_flt_dtype(){
	int flt_size=sizeof(i3_flt);
	if (flt_size==4) return TFLOAT;
	else if (flt_size==8) return TDOUBLE;
	else {
		fprintf(stderr,"ERROR: im3shape was COMPILED WRONGLY.\nThe i3_image header should define i3_flt to be either a 32 or 64 bit floating point type.  Instead it is of size %d.\n  FITSIO cannot read cope with any other floating point sizes as of now.\n",flt_size);
		exit(1);
	}
	
}


int i3_array_i3_flt_into_float(i3_flt * src, float * dst, int n){

	if(src == NULL || dst == NULL) {return I3_MATH_ERROR;};

	for(int i=0;i<n;i++) dst[i] = (float)src[i];

	return 0;
	
}


int i3_array_float_into_i3_flt(float * src, i3_flt * dst, int n){

	if(src == NULL || dst == NULL) {return I3_MATH_ERROR;};

	for(int i=0;i<n;i++) dst[i] = (i3_flt)src[i];

	return 0;
	
}