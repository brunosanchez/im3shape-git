#ifndef _H_I3_VERSION
#define _H_I3_VERSION
// This set of pre-processor macros defines HG_VERSION_STRING to be a string constant "..." 
// containing the value of the macro HG_VERSION.  I do it this way to avoid having to mess about with 
// multiple sets of quotes in the Makefile.

//If this file is compiled with -D"xxxyyyzzz" then you can use HG_VERSION_STRING to mean "xxxyyyzzz" in any file that includes this.

#ifdef HG_VERSION
#define HG_QUOTE(str) #str
#define HG_EXPAND_AND_QUOTE(str) HG_QUOTE(str)
#define HG_VERSION_STRING HG_EXPAND_AND_QUOTE(HG_VERSION)
#else
#warning Version string unknown.  Compile with -DHG_VERSION="my_version_string" to enable.
#define HG_VERSION_STRING "unknown"
#endif



#endif
