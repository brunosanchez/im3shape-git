#include "i3_utils.h"
#include "i3_version.h"

int i3_max_string_length_export = i3_max_string_length;

bool i3_file_exists(char * filename)
{
	FILE * f = fopen(filename,"r");
	if (!f) return false;
	fclose(f);
	return true;
}


const char * i3_get_version()
{
	return HG_VERSION_STRING;
}


#ifdef LAPACK_PATH_CHECK
extern int dgesvd_(char *jobu, char *jobvt, int *m, int *n, double *a, 
  int *lda, double *s, double *u, int *ldu, double *vt, int *ldvt,
  double *work, int *lwork, int *info);

#include <dlfcn.h>
int i3_lapack_path_check()
{
  Dl_info info;
  int status = dladdr(dgesvd_, &info);
  if (status){
    printf("Lapack function dgesvd was found:\n");
    printf("path = %s\n", info.dli_fname);
    printf("sname = %s\n", info.dli_sname);
  }
  else{
    printf("Lapack function dgesvd was NOT found.\n");
  }
  return !status;
}


// end lapack path check
#endif 