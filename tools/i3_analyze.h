#ifndef _H_I3_ANALYZE
#define _H_I3_ANALYZE
#include "i3_global.h"
#include "i3_options.h"
#include "i3_image.h"
#include "i3_great.h"
#include "i3_model.h"
#include "i3_minimizer.h"
#include "i3_catalog.h"

#define MAX_NUMBER_PARAMS 32
#define MAX_NP2 (32*32)

typedef struct i3_result {
        gal_id  identifier;
        i3_flt time_taken;
        i3_parameter_set * params;
        i3_flt fluxes[2];
        i3_flt flux_ratio;
        i3_flt stats_signal_to_noise;
        i3_flt stats_old_signal_to_noise;
        i3_flt stats_min_residuals;
        i3_flt stats_max_residuals;
        i3_flt stats_model_min;
        i3_flt stats_model_max;
        i3_flt likelihood;
        i3_flt levmar_residual_error_at_start;
        i3_flt levmar_residual_error_at_end;
        i3_flt levmar_maximal_gradient_projected_residual;
        i3_flt levmar_difference_in_parameter_vector;
        i3_flt levmar_difference_in_residual_error;
        i3_flt levmar_maximal_gradient_component;
        unsigned long levmar_number_of_iterations;
        unsigned long levmar_reason_of_termination;
        unsigned long levmar_number_of_likelihood_evaluations;
        unsigned long levmar_number_of_gradient_evaluations;
        unsigned long levmar_number_of_linear_systems_solved;
        i3_flt exposure_x[MAX_N_EXPOSURE];
        i3_flt exposure_y[MAX_N_EXPOSURE];
        i3_flt exposure_e1[MAX_N_EXPOSURE];
        i3_flt exposure_e2[MAX_N_EXPOSURE];
        i3_flt exposure_chi2[MAX_N_EXPOSURE];
        i3_flt exposure_residual_stdev[MAX_N_EXPOSURE];
        i3_flt exposure_background[MAX_N_EXPOSURE];
        char exposure_band[MAX_N_EXPOSURE];
        i3_flt mean_flux;
        int number_varied_params;
        i3_flt covariance_estimate[MAX_NP2];

} i3_result;

i3_data_set * i3_build_dataset_truncated_psf(i3_options * options, gal_id  identifier, i3_image * image, i3_image * weight, i3_moffat_psf * psf_form, int psf_type, i3_flt truncation);
i3_data_set * i3_build_dataset(i3_options * options, gal_id  identifier, i3_image * image, i3_image * weight, i3_moffat_psf * psf_form, int psf_type);
i3_data_set * i3_build_dataset_with_psf_image(i3_options * options, gal_id  identifier, i3_image * image, i3_image * weight, i3_image * psf_image);

// void save_model_data_images(i3_image * model_image, i3_image * data, char * output_dir, char * filetail);
// void i3_analyze_dataset(i3_data_set * data_set, i3_model * model, i3_options * options, FILE * output_file, int identifier);
// void i3_fiducial_parameters(i3_milestone_parameter_set * params, i3_moffat_psf * psf, int nx);
//void save_result_im3shape(i3_model * model, i3_parameter_set * params, i3_flt likelihood, FILE * output_file, int identifier, i3_data_set * dataset, i3_flt min_residuals, i3_flt max_residuals);
void copy_result_im3shape_to_struct(i3_options * options, i3_model * model, i3_parameter_set * params_input, i3_flt likelihood, i3_result * result, gal_id  identifier, i3_data_set * dataset, i3_flt min_residuals, i3_flt max_residuals, i3_flt model_min, i3_flt model_max, double time_taken, i3_flt * image_info);
void save_result_im3shape(i3_options * options, i3_model * model, i3_parameter_set * params, i3_flt likelihood, FILE * output_file, i3_catalog_row * row, i3_data_set * dataset, i3_flt min_residuals, i3_flt max_residuals, i3_flt model_min, i3_flt model_max, double time_taken, i3_flt * image_info);

// void i3_analyze_dataset_method(i3_data_set * data_set, i3_model * model, i3_options * options, FILE * output_file, int identifier, i3_minimizer_method method);
i3_parameter_set * i3_analyze_dataset_method(i3_data_set * data_set, i3_model * model, i3_options * options, i3_flt * best_like, i3_image * best_image, i3_minimizer_method method);
i3_parameter_set * i3_analyze_dataset(i3_data_set * data_set, i3_model * model, i3_options * options, i3_flt * best_like, i3_image * best_image);
const char * i3_analyze_sersics_header_line();


#endif


