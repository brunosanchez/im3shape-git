#include "math.h"

typedef double (*praxis_function)(double *,void * );
int i3_praxis_minimization(praxis_function f, double * start_position, int max_iterations, double * minima, double * maxima, void * args, int n, double tolerance, double * minimum_value, double * minimum_location, int verbosity);


