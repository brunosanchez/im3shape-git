#include "i3_gsl_random.h"
#include "time.h"
#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"
#include "stdio.h"
gsl_rng * RNG=NULL;



void i3_gsl_init_rng(){
	i3_gsl_init_rng_multiprocess(0);
}

void i3_gsl_init_rng_multiprocess(int i){
	RNG = gsl_rng_alloc(gsl_rng_taus2);
	gsl_rng_set(RNG,time(NULL)+i);
}

void i3_gsl_init_rng_multiprocess_environment(int i){
	gsl_rng_env_setup();
	RNG = gsl_rng_alloc(gsl_rng_taus2);
	gsl_rng_set(RNG,gsl_rng_default_seed+i);
}

void i3_gsl_init_rng_environment(){
	i3_gsl_init_rng_multiprocess_environment(0);
}

void i3_gsl_init_rng_fixedseed(int i){
        gsl_rng_set(RNG,i);
}



float i3_gsl_random_uniform_float(){
	return (float) gsl_rng_uniform(RNG);
}

double i3_gsl_random_uniform_double(){
	return gsl_rng_uniform(RNG);
}

float i3_gsl_random_normal_float(){
	return (float)gsl_ran_gaussian_ziggurat (RNG,1.0);	
}

double i3_gsl_random_normal_double(){
	return gsl_ran_gaussian_ziggurat (RNG,1.0);
}

