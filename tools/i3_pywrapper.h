#ifndef _H_I3_PYWRAPPER
#define _H_I3_PYWRAPPER
#include "i3_mcmc.h"
#include "i3_load_data.h"
#include "i3_math.h"
#include "i3_model.h"
#include "i3_minimizer.h"
#include "i3_fisher.h"
#include "i3_mcmc.h"
#include "i3_utils.h"
#include "i3_great.h"
#include "i3_image_fits.h"
#include "i3_analyze.h"
#include "models/i3_sersics.h"
#include "i3_version.h"
#include <time.h>

void i3_analyze_wrapper(i3_image * galaxy, i3_image * psf_image, 
	i3_image * weight_image, i3_image * mask_image, i3_image * bestI, i3_options * options, 
	gal_id identifier, i3_flt x, i3_flt y, i3_result * result, int starting_params_only);

#endif
