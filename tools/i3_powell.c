#include "i3_powell.h"
#include "stdlib.h"
#include "math.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_min.h>
#include "i3_mcmc.h"
#include "i3_math.h"
#include "i3_model.h"
#include "float.h"


#define I3_POWELL_MAX_LINE_JUMP 1.0
#define I3_POWELL_EQUAL_VALUES 1e-5

typedef struct i3_powell_wrapper_data{
	powell_function f;
	float * start;
	float * direction;
	float * min;
	float * max;
	float * scales;
	void * args;
	int n;
} i3_powell_wrapper_data;


void fprint_matrix(FILE * output,float ** M,int n){
	for (int i=0;i<n;i++){
		for (int j=0;j<n;j++){
			fprintf(output,"%f\t",M[i][j]);
		}
		fprintf(output,"\n");
	}
	
}



int i3_powell_ncall=0;

double i3_powell_line_wrapper(double x, void * params){
	i3_powell_wrapper_data * data=(i3_powell_wrapper_data*) params;
	i3_powell_ncall++;
	float p[data->n];
	for(int i=0;i<data->n;i++) p[i]=data->min[i] + (data->start[i]+data->direction[i]*x) * data->scales[i];
	float result = data->f(p,data->args);
	// printf(" +++  %f  %f\n",x,result);
	return (double) result;
}


void i3_powell_bracket(i3_powell_wrapper_data * data, int max_iterations, double * lower_jump, double * upper_jump, double rescale){


	/* If we rescale all the dimensions of the box defined by the minimum and maximum parameter values then the maximum
	 distance we might jump is i3_sqrt(n), which is from one corner of the box to the other.
	 */
	float maximum_jump_length = i3_sqrt(1.0*data->n)*1.001;

	//bracket upper
	double base = i3_powell_line_wrapper(0.0,data);
	double upper=i3_powell_line_wrapper(maximum_jump_length,data);
	double scale = 1.0;

	double new_upper=upper;
	
	int iterations = 0;
	while(new_upper>base && iterations<max_iterations){
		upper=new_upper;
		scale*=rescale;
		iterations++;
		new_upper=i3_powell_line_wrapper(maximum_jump_length*scale,data);
	}
	*upper_jump = maximum_jump_length*scale/rescale;

	
	
	double lower=i3_powell_line_wrapper(-maximum_jump_length,data);
	scale=1.0;
	double new_lower=lower;
	iterations = 0;
	while(new_lower>base && iterations<max_iterations){
		lower=new_lower;
		scale*=rescale;
		iterations++;
		new_lower=i3_powell_line_wrapper(-maximum_jump_length*scale,data);
	}
	*lower_jump = -maximum_jump_length*scale/rescale;
	
}





int i3_powell_linmin(i3_powell_wrapper_data * data, float * min_val, float * min_pos, float tolerance){

	bool all_zero = true;
	for (int i=0;i<data->n;i++) if (data->direction[i]!=0) all_zero=false;
	if (all_zero){
		*min_val = 0.0;
		*min_pos = 0.0;
		return 1;
	}

	
	fprintf(stderr,"LINMIN Direction: [");
	for(int i=0;i<data->n;i++) fprintf(stderr,"%f\t",data->direction[i]);
	fprintf(stderr,"]\n");
	fprintf(stderr,"LINMIN Start: [");
	for(int i=0;i<data->n;i++) fprintf(stderr,"%f\t",data->start[i]);
	fprintf(stderr,"]\n");
	
	
	/* We wrap the function so that we can pass it to the gsl library. */
	gsl_function F;
	F.function=&i3_powell_line_wrapper;
	F.params=data;
	

	double bracketing_scale = 0.5;

	double jump_upwards, jump_downwards;
	i3_powell_bracket(data, 4, &jump_downwards, &jump_upwards,bracketing_scale);
	// printf("Jump up  :%e\nJump down:%e\n",jump_upwards,jump_downwards);	


	gsl_min_fminimizer * minimizer_engine = gsl_min_fminimizer_alloc(gsl_min_fminimizer_quad_golden);
	gsl_error_handler_t * error_handler = gsl_set_error_handler_off();
	int status;
	status = gsl_min_fminimizer_set(minimizer_engine,&F,0.0,jump_downwards,jump_upwards);
	if (status==GSL_EINVAL) {
		// Try one more time, with wider ranges.
		status = gsl_min_fminimizer_set(minimizer_engine,&F,0.0,jump_downwards/bracketing_scale,jump_upwards/bracketing_scale);
	}
	if (status) {
		float L = jump_downwards/bracketing_scale;
		float H = jump_upwards/bracketing_scale;
		float FL = i3_powell_line_wrapper(L,data);
		float FH = i3_powell_line_wrapper(H,data);
		float F0 = i3_powell_line_wrapper(0.0f,data);
		
		fprintf(stderr,"]\nMinimizer Problem:\n");
		fprintf(stderr,"F(0) = %f\n",F0);
		fprintf(stderr,"[ ");
		for(int i=0;i<data->n;i++) fprintf(stderr,"%f\t",data->min[i] + (data->start[i]+data->direction[i]*0.0) * data->scales[i]);
		fprintf(stderr,"]\n");
		
		fprintf(stderr,"F(%f) = %f\n",L,FL);
		fprintf(stderr,"[ ");
		for(int i=0;i<data->n;i++) fprintf(stderr,"%f\t",data->min[i] + (data->start[i]+data->direction[i]*L) * data->scales[i]);
		fprintf(stderr,"]\n");
		
		fprintf(stderr,"F(%f) = %f\n",H,FH);
		fprintf(stderr,"[ ");
		for(int i=0;i<data->n;i++) fprintf(stderr,"%f\t",data->min[i] + (data->start[i]+data->direction[i]*H) * data->scales[i]);
		fprintf(stderr,"]\n");
	}
	
	int i;
	
	if (status!=GSL_EINVAL){
		for(i=0;i<INNER_ITERATIONS;i++){
			gsl_min_fminimizer_iterate(minimizer_engine);
	        double a = gsl_min_fminimizer_x_lower (minimizer_engine);
	        double b = gsl_min_fminimizer_x_upper (minimizer_engine);
	        status = gsl_min_test_interval (a, b, tolerance, 0.0);
			if (status==GSL_EBADFUNC) break;
			if (status==GSL_SUCCESS) break;
		}
	}
	// printf("Inner: %d iterations (bad:%d,succ:%d,einval:%d)\n",i,status==GSL_EBADFUNC,status==GSL_SUCCESS,status==GSL_EINVAL);
	if (status==GSL_EBADFUNC||status==GSL_EINVAL){
		*min_val = gsl_min_fminimizer_f_minimum(minimizer_engine);
		*min_pos = 0.0;
	}
	else{
		*min_val = gsl_min_fminimizer_f_minimum(minimizer_engine);
		*min_pos = gsl_min_fminimizer_x_minimum(minimizer_engine);
	}
	// printf("Result:%f\n\n",*min_pos);

	if (*min_pos==0){
		fprintf(stderr,"Jump = 0  (status = %d, iterations = %d)\n",status,i);
	}

	gsl_min_fminimizer_free(minimizer_engine);
	gsl_set_error_handler (error_handler);
	return status;	
}




void setup_directions(float *** directions, int n){
	*directions = (float**)malloc(n*sizeof(float*));
	for(int j=0;j<n;j++){
		(*directions)[j]=(float*)malloc(n*sizeof(float));
	}
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(i==j)
				(*directions)[i][j]=1.0;
			else
				(*directions)[i][j]=0.0;
		}
	}	
}

void setup_positions(float *** positions, int n){
	*positions = (float**)malloc(n*sizeof(float*));
	for(int j=0;j<n;j++){
		(*positions)[j]=(float*)malloc(n*sizeof(float));
	}
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
				(*positions)[i][j]=0.0;
		}
	}	
}


int i3_powell_minimization(powell_function f, float * start_pos, float * param_min, float * param_max, void * args, int n, int max_it, float ftol, float * result, float * min_pos){
	float ** directions;
	float ** positions;
	float * scales = malloc(n*sizeof(float));
	for (int k=0;k<n;k++) scales[k] = param_max[k]-param_min[k];
	
	i3_powell_wrapper_data data;
	data.f = f;
	data.start=NULL;
	data.direction=NULL;
	data.args=args;
	data.min=param_min;
	data.max=param_max;
	data.scales = scales;
	data.n=n;
	
	fprintf(stderr,"Error GSL_EINVAL = %d\nError GSL_EBADFUNC = %d\n\n",GSL_EINVAL,GSL_EBADFUNC);
	setup_directions(&directions,n);
	fprintf(stderr,"Initial Directions = \n");
	fprint_matrix(stderr,directions,n);
	
	setup_positions(&positions,n);
	fprintf(stderr,"Initial Positions = \n");
	fprint_matrix(stderr,positions,n);
	float jump;
	float min_val;
	float * x0 = malloc(sizeof(float)*n);
	float * line_start = malloc(sizeof(float)*n);
	float * rescaled_start = malloc(sizeof(float)*n);
	for (int i=0;i<n;i++) rescaled_start[i] = (start_pos[i]-param_min[i])/scales[i];
	data.start=rescaled_start;
	data.direction=directions[n-1];
	int status = i3_powell_linmin(&data,&min_val,&jump,ftol);
	if (status){fprintf(stderr,"Linmin status(0) = %d\n",status);}
	
	
	
	for (int k=0;k<n;k++) x0[k] = rescaled_start[k] + directions[n-1][k]*jump;

	for (int i=0;i<max_it;i++){
		fprintf(stderr,"Powell iteration %d\n",i);
		for (int j=0;j<n;j++){
			if (j==0) for (int k=0;k<n;k++) line_start[k]=x0[k];
			else for (int k=0;k<n;k++) line_start[k]=positions[j-1][k];
			data.start=line_start;
			data.direction=directions[j];
			status = i3_powell_linmin(&data,&min_val,&jump,ftol);
			if (status){fprintf(stderr,"Linmin status(1) = %d\n",status);}
			for (int k=0;k<n;k++) positions[j][k]=line_start[k] + jump*directions[j][k];
			
			if (j<n-1) for (int k=0;k<n;k++) directions[j][k]=directions[j+1][k];
			fprintf(stderr,"Interior update. Directions = \n");
			fprint_matrix(stderr,directions,n);
		}
		fprintf(stderr,"Iterated positions = \n");
		fprint_matrix(stderr,positions,n);
		
		for (int k=0;k<n;k++) directions[n-1][k]=positions[n-1][k]-x0[k];
		fprintf(stderr,"Exterior update. Directions = \n");
		fprint_matrix(stderr,directions,n);
		
		data.start=x0;
		data.direction=directions[n-1];
		status = i3_powell_linmin(&data,&min_val,&jump,ftol);
		if (status){fprintf(stderr,"Linmin status(2) = %d\n",status);}
		for(int k=0;k<n;k++) fprintf(stderr,"Jump(%d) = %f\n",k,jump*directions[n-1][k]);
		
		for (int k=0;k<n;k++) x0[k] += jump*directions[n-1][k];
		fprintf(stderr,"-- %e\n",min_val);
	}


	for (int k=0;k<n;k++) min_pos[k] = param_min[k] + scales[k] * x0[k];
	data.start=x0;
	*result = i3_powell_line_wrapper(0,&data);
	
	for(int j=0;j<n;j++) free(directions[j]);
	free(directions);
	for(int j=0;j<n;j++) free(positions[j]);
	free(positions);
	free(scales);
	free(x0);
	free(rescaled_start);
	free(line_start);
	return status;
	
}


/*
int i3_powell_minimization(powell_function * f, float * start_pos, float * param_min, float * param_max, void * args, int n, int max_it, float ftol, float * result, float * min_pos){
	i3_powell_ncall=0;
	float p[n];
	float pt[n];
	float ptt[n];
	float direction[n];
	float scales[n];
	int status=0;
	for (int i=0;i<n;i++) scales[i]=param_max[i]-param_min[i]; //This is the upper limit to the distance we might need to move.  The real distance will be much smaller
	float min_val, fp, greatest_decrease, fptt, t;
	int best_direction;
	float **direction_set=(float**)malloc(n*sizeof(float*));
	for(int j=0;j<n;j++){
		direction_set[j]=(float*)malloc(n*sizeof(float));
	}
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(i==j)
				direction_set[i][j]=1.0;
			else
				direction_set[i][j]=0.0;
		}
	}
	
	for(int i=0;i<n;i++) p[i]=start_pos[i];
	float min_dist;
	min_val=f(p,args);
	for(int j=0;j<n;j++) pt[j]=p[j];
	int it;
	for(it=0;it<max_it;it++){
		fp=min_val;
		best_direction=0;
		greatest_decrease=0.0;
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++) direction[j]=direction_set[i][j];
			fptt=min_val;
			status = i3_powell_linmin(f,args,p,direction,scales,n,&min_val,&min_dist,ftol); //reset p and reset xit
			if (status) break;
			for(int k=0;k<n;k++){
				p[k]=p[k]+min_dist*direction[k]*scales[k];
				direction[k]=direction[k]*min_dist*scales[k];
			}
			if (i3_fabs(fptt-min_val)>greatest_decrease){
				greatest_decrease=i3_fabs(fptt-min_val);
				best_direction=i;
			}
		}
		if (status) break;
		float decrease_fraction = 2*i3_fabs(fp-min_val)/(i3_fabs(fp)+i3_fabs(min_val));
		
		if (fp-min_val==0 || decrease_fraction<ftol) {
			fprintf(stderr,"Powell: fp = %f,  min_val = %f,  decrease_fraction = %f\n");
			break; //Converged
		}
		for(int j=0;j<n;j++){
			ptt[j]=2*p[j]-pt[j];
			direction[j]=p[j]-pt[j];
			pt[j]=p[j];
		}
		
		fptt=f(ptt,args);
		if (fptt<fp){
			t=2*(fp-2*min_val+fptt)*i3_sqrt(fp-min_val-greatest_decrease)-greatest_decrease*i3_sqrt(fp-fptt);
			if (t<0){
				status=i3_powell_linmin(f,args,p,direction,scales,n,&min_val,&min_dist,ftol);
				if (status) break;
				for(int k=0;k<n;k++){
					p[k]=p[k]+min_dist*direction[k]*scales[k];
					direction[k]=direction[k]*min_dist*scales[k];
				}
				
				for(int j=0;j<n;j++){
					direction_set[best_direction][j]=direction_set[n-1][j];
					direction_set[n-1][j]=direction[j];
				}
			}
	
		}
	}
	fprintf(stderr,"Powell: %d\n",it);
	if (status) fprintf(stderr,"Powell failed:%d\n",status);
	fflush(stdout);
	for(int j=0;j<n;j++) free(direction_set[j]);
	free(direction_set);
	for(int i=0;i<n;i++) min_pos[i]=p[i];
	*result=min_val;
	if (it==max_it||status) return 1;
	return 0;
}
*/



