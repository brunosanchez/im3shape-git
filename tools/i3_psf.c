//I(r) = (1 + (r/r_d)^2 )^(-beta)
#include "i3_psf.h"
#include "gsl/gsl_sf_bessel.h"
#include "i3_image_fits.h"
#include "string.h"

void i3_image_add_moffat(i3_image * image, i3_flt x0, i3_flt y0, i3_flt radius, i3_flt e1, i3_flt e2, i3_flt beta){
	i3_image_add_truncated_moffat(image, x0, y0, radius, e1, e2, beta, FLT_MAX);
}


void i3_image_add_truncated_moffat(i3_image * image, i3_flt x0, i3_flt y0, i3_flt radius, i3_flt e1, i3_flt e2, i3_flt beta,i3_flt trunctation){
	i3_flt r_d2 = radius*radius;
	i3_flt mBeta = -beta;
	i3_flt t2 = trunctation*trunctation;
	
	
	i3_flt m1;
	i3_flt m2;
	i3_flt m3;
	i3_unit_shear_matrix(e1, e2, &m1, &m2, &m3);


	for(int i=0;i<image->nx;i++){
		for(int j=0;j<image->ny;j++){
				i3_flt x;
				i3_flt y;
         		i3_image_xy_from_ij(image, i, j, &x, &y);
				i3_flt dx = x - x0;
				i3_flt dy = y - y0;
			i3_flt sheared_r2 = m1*dx*dx + 2*m2*dx*dy + m3*dy*dy;
			if (sheared_r2<t2) image->row[j][i] += i3_pow(1+sheared_r2/r_d2,mBeta);
		}
	}
}


void i3_image_add_truncated_airy(i3_image * image, i3_flt x0, i3_flt y0, i3_flt fwhm, i3_flt e1, i3_flt e2,i3_flt trunctation){

	i3_flt airy_d = 1.028993969962188 / fwhm;

	// i3_flt r_d2 = radius*radius;
	i3_flt t2 = trunctation*trunctation;
	
	i3_flt m1;
	i3_flt m2;
	i3_flt m3;
	i3_unit_shear_matrix(e1, e2, &m1, &m2, &m3);

	
	for(int i=0;i<image->nx;i++){
		for(int j=0;j<image->ny;j++){
			i3_flt x;
			i3_flt y;
     		i3_image_xy_from_ij(image, i, j, &x, &y);
			i3_flt dx = x - x0;
			i3_flt dy = y - y0;

			i3_flt sheared_r2 = m1*dx*dx + 2*m2*dx*dy + m3*dy*dy;
			i3_flt sheared_r = i3_sqrt(sheared_r2);
			i3_flt sheared_x = M_PI*sheared_r * airy_d;
			i3_flt sheared_x2 = sheared_x*sheared_x;
			if (sheared_x<1e-12) image->row[j][i] += 1.0;
			if (sheared_r2<t2) image->row[j][i] += 4*pow(gsl_sf_bessel_J1(sheared_x),2)/sheared_x2;
		}   //	I(r) = I_0 * 4 * BesselJ1(\pi * d * r) / (\pi * d * r)
		
	}
}

//

void i3_image_add_truncated_pseudo_airy(i3_image * image, i3_flt x0, i3_flt y0, i3_flt r_d, i3_flt e1, i3_flt e2,i3_flt trunctation){
  /*
  This function generates the pseudo-Airy function defined by Kuijken (2006),
  used in the GREAT10 simulations as an Airy-like PSF model for which

  I(r) = I(x = r/r_d) = sin^2(x) / x^2 for x < 1
                      = sin^2(x) / x^3 for x > 1

  where the scale radius can be defined in terms of the FWHM as

  r_d = 0.5 * FWHM / 1.203

  */


	// i3_flt r_d2 = radius*radius;
	i3_flt t2 = trunctation*trunctation;
	
	i3_flt m1;
	i3_flt m2;
	i3_flt m3;
	i3_unit_shear_matrix(e1, e2, &m1, &m2, &m3);
	
	
	for(int i=0;i<image->nx;i++){
		for(int j=0;j<image->ny;j++){
			i3_flt x;
			i3_flt y;
     		i3_image_xy_from_ij(image, i, j, &x, &y);
			i3_flt dx = x - x0;
			i3_flt dy = y - y0;

			i3_flt sheared_r2 = m1*dx*dx + 2*m2*dx*dy + m3*dy*dy;
			i3_flt sheared_r = i3_sqrt(sheared_r2);
			i3_flt sheared_x = sheared_r / r_d;
			i3_flt sheared_x2 = sheared_x*sheared_x;
			i3_flt sheared_x3 = sheared_x2*sheared_x;
			if (sheared_x<1e-12) image->row[j][i] += 1.0;
			else if (sheared_x<1.0 && sheared_r2<t2) image->row[j][i] += pow(sin(sheared_x), 2) / sheared_x2;
			else if (sheared_x>=1.0 && sheared_r2<t2) image->row[j][i] += pow(sin(sheared_x), 2) / sheared_x3;
		}   //	I(r) = I_0 * 4 * BesselJ1(\pi * d * r) / (\pi * d * r)
		
	}
}



// 


i3_flt i3_moffat_fwhm(i3_flt d, i3_flt beta)
{
// The Moffat profile is:
// f(r) = (1+r^2/d^2)^(-beta)
// f(0)=1, so to compute the FWHM we want to solve f(r)=1/2
//    (1+r^2/d^2)^(-beta) = 1/2
// => (1+r^2/d^2)^beta = 2
// => 1+r^2/d^2 = 2^(1/beta)
// => r^2 = d^2 (2^(1/beta)-1)
// => r = d (2^(1/beta)-1)^(1/2)
// The FWHM is 2r.

	i3_flt r = d * i3_sqrt(i3_pow(2.,1./beta)-1);
	return 2*r;
}

i3_flt i3_moffat_radius(i3_flt fwhm, i3_flt beta)
{
	//Inverse of above function.
	i3_flt d = fwhm/2.0/i3_sqrt(i3_pow(2.,1./beta)-1);
	return d;
}




i3_flt i3_pseudo_airy_radius(i3_flt fwhm)
{
  // Calculates scale radius for pseudo-Airy as a function of input FWHM 
  // Taken from Kuijken (2006) & online Great10 documentation
  i3_flt rd = 0.5 * fwhm / 1.203;
  return rd;
}


i3_fourier * i3_fourier_downsampling_psf2(int nx_low_resolution, int ny_low_resolution, int upsampling)
{
	int nx = nx_low_resolution*upsampling;
	int ny = ny_low_resolution*upsampling;

	if (nx!=ny) I3_FATAL("Have not coded i3_fourier_downsampling_psf2 to use nx!=ny",1);
	
	i3_fourier * kernel = i3_fourier_create(nx,ny);
	i3_flt df=upsampling/2./nx;
	i3_flt k;
	
//	printf("%ld,%ld\n",kernel->nx_f,kernel->ny_f);
	for(int i=0;i<kernel->ny_f/2;i++){
		for(int j=0;j<kernel->nx_f;j++){
			k=df*i3_sqrt(i*1.*i+j*1.*j);
			kernel->row[i][j]=i3_sinc(M_PI*df*i)*i3_sinc(M_PI*df*j);
			//JAZ exploit the symmetry of the function hrere.
			kernel->row[kernel->ny_f-1-i][j]=kernel->row[i][j];
		}
		
	}
	return kernel;
}


i3_fourier * i3_fourier_downsampling_psf(int nx_low_resolution, int ny_low_resolution, int upsampling, int padding_low_resolution)
{
	
	int nx = nx_low_resolution*upsampling;
	int ny = ny_low_resolution*upsampling;
	int padding = padding_low_resolution*upsampling;
	i3_flt U = 1.0/(upsampling*upsampling);
	i3_image * image = i3_image_create(nx,ny);
	int xc = nx/2+1-(upsampling/2+1);
	int yc = ny/2+1-(upsampling/2+1);
	i3_image_zero(image);
	for(int i=0;i<upsampling;i++){
		for(int j=0;j<upsampling;j++){
			image->row[xc+i][yc+j]=U;
		}
	}

	i3_image * padded;
	if (upsampling && (padding_low_resolution != 0)){
		padded = i3_image_padded_corner(image,padding);
	}
	else{
		padded = image;
	}
//	printf("Padded PSF size = (%d,%d)\n",padded->nx,padded->ny);
	i3_fourier * ft = i3_image_fourier(padded);
	i3_image_destroy(image);
	if (upsampling && (padding_low_resolution != 0)) i3_image_destroy(padded);
	return ft;
	
	
	
}

i3_image * i3_image_get_top_hat(int nx_low_res, int ny_low_res, int sub){

	i3_flt U = 1.0/(sub*sub);
	int nx = nx_low_res*sub;
	int ny = ny_low_res*sub;
	i3_image * image = i3_image_create(nx,ny);
	i3_image_zero(image);
	int xc = nx/2-sub/2;
	int yc = ny/2-sub/2;
	for(int i=0;i<sub;i++){
		for(int j=0;j<sub;j++){
			image->row[xc+i][yc+j]=U;
		}
	}
	return image;

}





i3_image * i3_make_great10_psf(int N_pix, int N_sub, int N_pad, i3_flt psf_beta, i3_flt psf_fwhm, i3_flt psf_e1, i3_flt psf_e2, i3_flt psf_r_trunc, int psf_type){

	// stamp parameters

		int N_all = N_pad+N_pix; // total number of pixels, including padding, NOT including subpixels

	// psf parameters

		i3_flt psf_xy0 = N_all*N_sub/2 + 0.5;
		// printf("N_pix = %d psf_xy0 = %f\n",N_pix,psf_xy0);
		//		i3_flt psf_r = i3_moffat_radius(psf_fwhm*N_sub,psf_beta);
		// printf("beta = %f  fwhm = %f  trunc = %f   radius=%f\n",psf_beta, psf_fwhm, psf_r_trunc, psf_r);
		// printf("N_pix = %d,   N_all = %d,  N_pad = %d,  N_sub = %d\n", N_pix, N_all, N_pad, N_sub);
		i3_image * image_psf = i3_image_create(N_all*N_sub,N_all*N_sub);	i3_image_zero(image_psf);

		switch (psf_type){
			case GREAT10_PSF_TYPE_MOFFAT:
			  {i3_flt psf_r = i3_moffat_radius(psf_fwhm*N_sub,psf_beta);
		    i3_image_add_truncated_moffat( image_psf,psf_xy0,psf_xy0,psf_r,psf_e1,psf_e2,psf_beta,psf_r_trunc*N_sub);}
			break;
			case GREAT10_PSF_TYPE_AIRY:
			  {i3_flt psf_r = i3_pseudo_airy_radius(psf_fwhm*N_sub);
			  i3_image_add_truncated_pseudo_airy(image_psf, psf_xy0, psf_xy0, psf_r, psf_e1, psf_e2,psf_r_trunc*N_sub);}
			break;
			default:
			I3_FATAL("Unknown great10 PSF type - should be 0 for moffat or 1 for airy",1);
		}

		i3_flt flux = i3_image_sum(image_psf);
		i3_image_scale(image_psf,1.0/flux);

		return image_psf;
}

i3_fourier * i3_fourier_conv_kernel_moffat(int N_pix, int N_sub, int N_pad, i3_flt psf_beta, i3_flt psf_fwhm, i3_flt psf_e1, i3_flt psf_e2, i3_flt psf_r_trunc, int psf_type){

	i3_image * image_psf = i3_make_great10_psf(N_pix, N_sub, N_pad, psf_beta, psf_fwhm, psf_e1, psf_e2, psf_r_trunc, psf_type);
	i3_fourier * fourier_ker = i3_fourier_conv_kernel(N_pix, N_sub, N_pad, image_psf, 1); 
	i3_image_destroy( image_psf );
	return fourier_ker;


}

i3_fourier * i3_fourier_conv_kernel(int N_pix, int N_sub, int N_pad, i3_image * image_psf, bool perform_pixel_integration){
// First deal with case where we have to take into account pixel integration
	if(perform_pixel_integration){
// stamp parameters

	        int N_all = N_pad+N_pix; // total number of pixels, including padding, NOT including subpixels	

// get sinc
	        i3_image * image_pix = i3_image_get_top_hat( N_all,N_all,N_sub );
		i3_image * image_pix_c = i3_image_centered( image_pix );
		i3_fourier * fourier_pix = i3_image_fourier( image_pix_c );


// finish if no psf is used
		if(!image_psf){
	                i3_image_destroy( image_pix );
			i3_image_destroy( image_pix_c );
			return fourier_pix ; // if no psf is used, then only sinc will be returned as convolution kernel
		}

// get the psf image
		//i3_image * image_psf = i3_make_great10_psf(N_pix, N_sub, N_pad, psf_beta, psf_fwhm, psf_e1, psf_e2, psf_r_trunc, psf_type);
		i3_image * image_psf_c = i3_image_centered( image_psf );
		i3_fourier * fourier_psf = i3_image_fourier( image_psf_c );

// combine kernels	
		i3_fourier * fourier_ker = i3_fourier_create(N_all*N_sub,N_all*N_sub);
		// printf("ker size = %ld x %ld", fourier_ker->nx_real,fourier_ker->ny_real);
	
		i3_multiply_fourier_fourier_into(fourier_pix,fourier_psf,fourier_ker);
	
// preview if the images are pretty
		//i3_image_save_fits( image_psf, "image_psf.fits" );

// clean up
		//i3_image_destroy( image_psf );
		i3_image_destroy( image_psf_c );
		i3_image_destroy( image_pix );
		i3_image_destroy( image_pix_c );
		i3_fourier_destroy( fourier_pix );
		i3_fourier_destroy( fourier_psf );
       
		return fourier_ker;

// No treat case where effective PSF is given, i.e. no pixel integration needs to be done
	} else {
		if(!image_psf)
		        I3_FATAL("No PSF image provided for convolution!",1);
		
		i3_image * image_psf_c = i3_image_centered( image_psf );
		i3_fourier * fourier_ker = i3_image_fourier( image_psf_c );
// clean up
		i3_image_destroy( image_psf_c );
		return fourier_ker;
	}
}

i3_moffat_psf * i3_small_round_psf()
{
	i3_moffat_psf * psf_form = malloc(sizeof(i3_moffat_psf));
	psf_form->beta = 3.;
	psf_form->e1 = 0.;
	psf_form->e2 = 0.;
	psf_form->fwhm = 1.0;
	return psf_form;
}


int i3_parse_moffat_psf_parameter(i3_moffat_psf * psf, char * name, double value)
{
	int status = 0;
	if (0==strncmp(name,"psf_",4)) name=name+4;	

	if(0==strcmp(name,"e1")){
		psf->e1 = value;
	} else
	if(0==strcmp(name,"e2")){
		psf->e2 = value;
	} else
	if(0==strcmp(name,"beta")){
		psf->beta = value;
	} else
	if(0==strcmp(name,"fwhm")){
		psf->fwhm=value;
	} else
	if(0==strcmp(name,"x")){
		psf->x=(int)value;
	} else
	if(0==strcmp(name,"y")){
		psf->y=(int)value;
	} else{
		status=1;
	}
	return status;
}
