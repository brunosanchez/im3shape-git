#include "i3_wcstools.h"
#include "fitsio.h"

i3_transform i3_get_wcs_to_image_transform(i3_flt ra, i3_flt dec, struct WorldCoor * w){

  /*
   * Outputs an i3_transform structure containing:
   *
   * transform.x0    Reference 'origin' of locally linear transformation in X_IMAGE
   * transform.y0    Reference 'origin' of locally linear transformation in Y_IMAGE
   * transform.ra0   Reference 'origin' of locally linear transformation in ALPHA_J2000 (degrees)
   * transform.dec0  Reference 'origin' of locally linear transformation in DELTA_J2000 (degrees)
   *
   * transform.A  This is a 2x2 matrix that stores the locally linear transformation in the
   *              direction sky -> image:
   *              A[0][0] = d(X_IMAGE) / d(ALPHA_J2000) / cosine(transform.dec0)
   *              A[0][1] = d(X_IMAGE) / d(DELTA_J2000)
   *              A[1][0] = d(Y_IMAGE) / d(ALPHA_J2000) / cosine(transform.dec0)
   *              A[1][1] = d(Y_IMAGE) / d(DELTA_J2000)
   */

	double x0, y0, dxdRA, dydRA, dxdDEC, dydDEC; // intermediates to locally linear solutions
	int out_of_range = 0;

	//Compute the pixel (x,y) coordinates for the three (ra,dec) points we need to make the transform
	wcs2pix(w, ra,               dec,               &x0, &y0, &out_of_range);
	wcs2pix(w, ra + I3_WCS_STEP, dec,               &dxdRA, &dydRA, &out_of_range);
	wcs2pix(w, ra,               dec + I3_WCS_STEP, &dxdDEC, &dydDEC, &out_of_range);
	
	if (out_of_range){
		fprintf(stderr,"There was an error converting WCS to image coordinates.  The ra, dec used were out of range.\n");
		return get_null_transform();
	}

	const double INVCOSDEC = 1. / i3_cos(M_PI * dec / 180.);
	i3_transform transform;
	transform.ra0 = ra;
	transform.dec0 = dec;
	transform.x0 = x0;
	transform.y0 = y0;
	transform.A[0][0] = (dxdRA - x0) * INVCOSDEC / I3_WCS_STEP;
	transform.A[0][1] = (dxdDEC - x0) / I3_WCS_STEP;
	transform.A[1][0] = (dydRA - y0) * INVCOSDEC / I3_WCS_STEP;
	transform.A[1][1] = (dydDEC - y0) / I3_WCS_STEP;
	return transform;

}

i3_transform i3_get_image_to_wcs_transform(i3_flt x, i3_flt y, struct WorldCoor * w){

  /*
   * Outputs an i3_transform structure containing:
   *
   * transform.x0    Reference 'origin' of locally linear transformation in X_IMAGE
   * transform.y0    Reference 'origin' of locally linear transformation in Y_IMAGE
   * transform.ra0   Reference 'origin' of locally linear transformation in ALPHA_J2000 (degrees)
   * transform.dec0  Reference 'origin' of locally linear transformation in DELTA_J2000 (degrees)
   *
   * transform.A  This is a 2x2 matrix that stores the locally linear transformation in the
   *              direction image -> sky:
   *              A[0][0] = d(ALPHA_J2000) * cosine(transform.dec0) / d(X_IMAGE)
   *              A[0][1] = d(ALPHA_J2000) * cosine(transform.dec0) / d(Y_IMAGE)
   *              A[1][0] = d(DELTA_J2000) / d(X_IMAGE)
   *              A[1][1] = d(DELTA_J2000) / d(Y_IMAGE)
   */													
        double RA0, DEC0, dRAdx, dDECdx, dRAdy, dDECdy;// intermediates to locally linear solutions
	pix2wcs(w, x,                 y,                 &RA0, &DEC0 );
	pix2wcs(w, x + I3_IMAGE_STEP, y,                 &dRAdx, &dDECdx );
	pix2wcs(w, x,                 y + I3_IMAGE_STEP, &dRAdy, &dDECdy );	
	
	const double COSDEC = i3_cos(M_PI * DEC0 / 180.);
	i3_transform transform;
	transform.ra0  = RA0;
	transform.dec0 = DEC0;
	transform.cosdec0 = COSDEC;
	transform.x0 = x;
	transform.y0 = y;
	transform.A[0][0] = (dRAdx - RA0) * COSDEC / I3_IMAGE_STEP;
	transform.A[0][1] = (dRAdy - RA0) * COSDEC / I3_IMAGE_STEP;
	transform.A[1][0] = (dDECdx - DEC0) / I3_IMAGE_STEP;
	transform.A[1][1] = (dDECdy - DEC0) / I3_IMAGE_STEP;
	return transform;

}
