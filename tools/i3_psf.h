#ifndef _H_I3_PSF
#define _H_I3_PSF

#include "stdbool.h"
#include "i3_global.h"
#include "i3_math.h"
#include "i3_image.h"


#define GREAT10_PSF_TYPE_MOFFAT 0
#define GREAT10_PSF_TYPE_AIRY 1
#define NO_PSF -1


typedef struct i3_moffat_psf{
	i3_flt beta;
	i3_flt fwhm;
	i3_flt e1;
	i3_flt e2;
	int x;
	int y;
} i3_moffat_psf;



/**
 * Add a sheared moffat profile to the given image.
 *
 * The Moffat profile is given by f(r) = (1+r^2/d^2)^(-beta).  We shear by changing what r means.
 * This function is really a call to i3_image_add_truncated_moffat with the truncation radius set very high.
 * 
 * /param image The image.  Do not forget to zero it beforehand if you need to.
 * /param x0 The x coordinate of the profile center.
 * /param y0 The y coordinate of the profile center.
 * /param radius The radius d of the profile.  To convert to this from the FWHM use the i3_moffat_radius function.
 * /param e1 The x-y ellipticity of the profile.
 * /param e2 The 45 degree ellipticity of the profile.
 * /param beta The beta slope parameter.
**/
void i3_image_add_moffat(i3_image * image, i3_flt x0, i3_flt y0, i3_flt radius, i3_flt e1, i3_flt e2, i3_flt beta);

/**
 * Add a sheared, truncated moffat profile to the image, in the style of Great08.
 * 
 * The parameters are the same as those for i3_image_add_moffat, but with an additional truncation radius.
 *
 * /param truncation The radius at which to truncate the profile to zero.
**/
void i3_image_add_truncated_moffat(i3_image * image, i3_flt x0, i3_flt y0, i3_flt radius, i3_flt e1, i3_flt e2, i3_flt beta,i3_flt trunctation);

/*
 * Generate the sinc fourier kernel needed to simulate pixel integration.  Convolve with this followed by pulling out a grid of pixels to downsample quickly.
 * 
 * The kernel is actually generated numerically by transforming a top-hat function, to ensure no surprises.
 *
 * /param nx_low_resolution  The x-size of the low-res image (i.e. the size of the image after downsampling).
 * /param ny_low_resolution   The y-size of the low-res image.
 * /param upsampling The ratio of the size of the high-res image to the low-res one.
 * /param padding The amount of zero padding to add to the image.
**/
i3_fourier * i3_fourier_downsampling_psf(int nx_low_resolution, int ny_low_resolution, int upsampling, int padding);

i3_fourier * i3_fourier_downsampling_psf2(int nx_low_resolution, int ny_low_resolution, int upsampling);


i3_image * i3_image_get_top_hat(int nx_low_res, int ny_low_res, int sub);


i3_flt i3_moffat_radius(i3_flt fwhm, i3_flt beta);
i3_flt i3_moffat_fwhm(i3_flt d, i3_flt beta);

/**
 * Get a combined PSF and subpixel integration kernel.
 * \param N_pix - number of pixels in the observed image (no padding, no subpixels)
 * \param N_sub - number of subpixels per pixel
 * \param N_pad - number of padding pixels. This number has to be odd. N_pad=2 means that each side of the image will be padded by 1.
 * \param psf_beta - Moffat beta parameter.
 * \param psf_fwhm - Moffat fwhm parameter.
 * \param psf_e1 - Moffat e1 parameter.
 * \param psf_e2 - Moffat e2 parameter.
 * \param psf_r_trunc - Moffat truncation radius.
 * \param psf_type - type of the PSF, 0 for Moffat, 1 for Airy, maybe later more.
 * \return Fourier image with max pixel in the top left corner of postage stamp.
**/
i3_fourier * i3_fourier_conv_kernel_moffat(int N_pix, int N_sub, int N_pad, i3_flt psf_beta, i3_flt psf_fwhm, i3_flt psf_e1, i3_flt psf_e2, i3_flt psf_r_trunc, int psf_type);

/**
 * Get a combined PSF and subpixel integration kernel. If empty pointer is passed as image_psf, then returns just pixel integration kernel.
 * \param N_pix - number of pixels in the observed image (no padding, no subpixels)
 * \param N_sub - number of subpixels per pixel
 * \param N_pad - number of padding pixels. This number has to be odd. N_pad=2 means that each side of the image will be padded by 1.
 * \param image_psf - image of the psf with image dimensions as in N_pix, N_sub, N_pad
 * \param perform_pixel_integration - whether to perform pixel integration, for psfex and shaplets set to false
 * \return Fourier image with max pixel in the top left corner of postage stamp.
**/
i3_fourier * i3_fourier_conv_kernel(int N_pix, int N_sub, int N_pad, i3_image * image_psf, bool perform_pixel_integration);


void i3_image_add_truncated_airy(i3_image * image, i3_flt x0, i3_flt y0, i3_flt fwhm, i3_flt e1, i3_flt e2, i3_flt trunctation);


/*
  This function generates the pseudo-Airy function defined by Kuijken (2006),
  used in the GREAT10 simulations as an Airy-like PSF model for which

  I(r) = I(x = r/r_d) = sin^2(x) / x^2 for x < 1
                      = sin^2(x) / x^3 for x > 1

  where the scale radius can be defined in terms of the FWHM as

  r_d = 0.5 * FWHM / 1.203

  */
i3_flt i3_pseudo_airy_radius(i3_flt fwhm);

void i3_image_add_truncated_pseudo_airy(i3_image * image, i3_flt x0, i3_flt y0, i3_flt r_d, i3_flt e1, i3_flt e2, i3_flt trunctation);

//This should really be in i3_great.c
i3_image * i3_make_great10_psf(int N_pix, int N_sub, int N_pad, i3_flt psf_beta, i3_flt psf_fwhm, i3_flt psf_e1, i3_flt psf_e2, i3_flt psf_r_trunc, int psf_type);

i3_moffat_psf * i3_small_round_psf();

int i3_parse_moffat_psf_parameter(i3_moffat_psf * psf, char * name, double value);

#endif

