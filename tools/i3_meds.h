#ifndef _H_I3_MEDS
#define _H_I3_MEDS

#include "i3_global.h"
#include "i3_image.h"
#include "i3_options.h"
#include "i3_analyze.h"
#include "i3_wcs.h"

i3_data_set * i3_build_multiple_exposure_dataset(i3_options * options, 
    gal_id identifier, int n_exposure, i3_image ** images, i3_image ** weights,
    i3_image ** psf_images, i3_transform * transforms, char * bands);

void i3_multiple_exposure_dataset_destroy(i3_data_set * dataset);

#endif
