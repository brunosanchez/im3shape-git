#ifndef _H_I3_GLOBAL
#define _H_I3_GLOBAL

#include <stdio.h>
#include <stdlib.h>

#include "i3_logging.h"
#ifndef __APPLE__
#include "alloca.h"
#endif

#ifdef __INTEL_COMPILER
#pragma warning disable 810
#endif

/* Our current images are 4-byte floats, but we may come to use 8-byte numbers
	depending on data formats.  To stay general we use i3_flt throughout this 
	code so we can just change it here if need be.

*/

#ifndef off_type
#define off_type long int
#endif

#define gal_id long int

/**
 * If the compilation flag I3_USE_DOUBLE is defined then i3_flt is double.  Otherwise (usual case) it is float.
**/
#ifdef I3_USE_DOUBLE
#define i3_flt double
#define i3_log log
#define i3_exp exp
#define i3_pow pow
#define i3_sin sinf
#define i3_cos cosf
#define i3_atan2 atan2
#define i3_sqrt sqrt
#define i3_cabs cabs
#define i3_fabs fabs
#define I3_ENSURE_FLOAT I3_FATAL("You have reached a piece of code that cannot cope with I3_USE_DOUBLE",1)
#else
#define i3_flt float
#define i3_log logf
#define i3_exp expf
#define i3_pow powf
#define i3_sin sinf
#define i3_cos cosf
#define i3_atan2 atan2f
#define i3_sqrt sqrtf
#define i3_cabs cabsf
#define I3_ENSURE_FLOAT {}
#define i3_fabs fabsf 
#endif

extern const int i3UseDouble;

#ifdef I3_FFTW_USE_DOUBLE
#define i3_cpx fftw_complex
#define i3_fftw_plan_dft_r2c_2d fftw_plan_dft_r2c_2d
#else
#define i3_cpx fftwf_complex
#define i3_fftw_plan_dft_r2c_2d fftwf_plan_dft_r2c_2d
#endif


/**
 * A convenient maximum string length for filenames, etc.
**/
#define i3_max_string_length 1024


#define I3_OK 0
#define I3_PATH_ERROR 1
#define I3_IMAGE_FAIL 1

typedef struct i3_mcmc * i3_mcmc_ptr;

/**
 * The generic parameter set.  This is void so it can represent any one of several different parameter sets.
**/
typedef void i3_parameter_set;




#endif


