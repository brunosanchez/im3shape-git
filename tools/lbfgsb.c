#include "stdio.h"
#include "stdlib.h"
#include "lbfgsb.h"
#include "string.h"
#include "math.h"


void lbf_copyCStrToCharArray (const char* source, char* dest, int ndest) {
  
  // Get the length of the source C string.
  int nsource = strlen(source);
  
  // Only perform the copy if the source can fit into the destination.
  if (nsource < ndest) {

    // Copy the string.
    strcpy(dest,source);

    // Fill in the rest of the string with blanks.
    for (int i = nsource; i < ndest; i++)
      dest[i] = ' ';
  }
}


int lbf_solve(int n, double * x0, double * lb, double * ub, lbf_function function, void * auxdata, int verbosity, int max_iterations, double accuracy)
{
	
	enum SolverExitStatus status = success;  // The return value.
	double value;
	double gradient[n];
	char task[60];
	int m = 3;
	int bounds_types[n];
//	double machine_precision = pow(2,-24);
	double relative_accuracy = accuracy;
	double gradient_accuracy = 1.0e-10;
	// double g[n];
	double wa[(2*m + 4)*n + 12*m*(m + 1)];
	int iwa[3*n];
	int lsave[4];
	int isave[44];
	char csave[60];
	double dsave[29];
	double x[n];
	
	for(int i=0;i<n;i++){
		int has_lb = (lb[i] != -INFINITY);
		int has_ub = (ub[i] !=  INFINITY);
		
		if      (!(has_lb) && (!has_ub)) bounds_types[i] = 0;
		else if (has_lb    && (!has_ub)) bounds_types[i] = 1;
		else if (has_lb    && has_ub   ) bounds_types[i] = 2;
		else if ((!has_lb) && has_ub   ) bounds_types[i] = 3;
		else {printf("??? fail in LBF\n"); exit(666);}
		x[i] = x0[i];
		//printf("Parameter %d:  start: %lf  lb: %lf  ub:  %lf  (%d,%d)\n",i,x[i],lb[i],ub[i],has_lb,has_ub);
	}
	
	function(x,&value,gradient,auxdata);


	lbf_copyCStrToCharArray("START",task,60);
	setulb_(&n,&m,x0,lb,ub,bounds_types,&value,
	gradient,&relative_accuracy,&gradient_accuracy,wa,iwa,task,
	&verbosity,csave,lsave,isave,dsave);

  // Repeat until we've reached the maximum number of iterations.
  int t = 0;
  while (1) {

    // Do something according to the "task" from the previous call to
    // L-BFGS.
    if (!strncmp(task,"FG",2)) {
		function(x,&value,gradient,auxdata);
    } else if (!strncmp(task,"NEW_X",5)) {
      	if (t == max_iterations) {
			lbf_copyCStrToCharArray("STOP",task,60);
			setulb_(&n,&m,x,lb,ub,bounds_types,&value,
			gradient,&relative_accuracy,&gradient_accuracy,wa,iwa,task,
			&verbosity,csave,lsave,isave,dsave);
			break;
      }
    } else if (!strncmp(task,"CONV",4))
		// Process has converged; quit
		break;
	else if (!strncmp(task,"ABNO",4)) {
		//some kind of failure
		status = abnormalTermination;
		break;
	} else if (!strncmp(task,"ERORR",5)) {
      status = errorOnInput;
      break;
    }

    // Next iteration.
	setulb_(&n,&m,x,lb,ub,bounds_types,&value,
	gradient,&relative_accuracy,&gradient_accuracy,wa,iwa,task,
	&verbosity,csave,lsave,isave,dsave);
  }
  for(int i=0;i<n;i++) x0[i] = x[i];

  return status;
  
}
