#ifndef _I3_DES_H
#define _I3_DES_H
#include "i3_global.h"
#include "i3_image.h"

typedef struct i3_des_catalog
{
	//Should we have some kind of identifier here?
	int n;
	i3_flt * x;  // center
	i3_flt * y;  // center
	i3_flt * size;    // pixel size of region
} i3_des_catalog;

i3_des_catalog * i3_des_catalog_stars(char * filename, float class_star_threshold);
i3_des_catalog * i3_des_catalog_create(int n);
void i3_des_catalog_print(i3_des_catalog * cat, FILE * output_file);
int i3_des_catalog_save(i3_des_catalog * cat, char * filename);

i3_image * i3_des_catalog_get_raw_stamp(i3_des_catalog * cat, i3_image * image, int i);
i3_image * i3_des_catalog_get_stamp(i3_des_catalog * cat, i3_image * image, int i, int min_size);

#endif
