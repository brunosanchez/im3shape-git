import py3shape
from inspect import currentframe, getframeinfo
from postprocessing import ModelFitInformation
from py3shape.precomputed import FitsCatDirectory, GreatDesCatDirectory
from py3shape.analyze import  convert_psf_ellipticities_to_wcs, count_varied_params
from py3shape.common import MAX_N_EXPOSURE
from py3shape.i3meds import I3MEDS, I3NBCMEDS, I3MultiExposureInputs
from py3shape.image import Image
from py3shape.output import TextOutput, DatabaseOutput
from py3shape.errors import *
from py3shape import utils
import sys
import os
import numpy as np
import argparse
import logging
import time
import meds
import traceback
from py3shape import structs

DES_PIXEL_SCALE = 0.27

def nancount(x):
    return x.size - np.isfinite(x).sum()

class I3Analysis(object):
    def __init__(self, options):
        super(I3Analysis, self).__init__()
        self.options = options
        self.pixel_scale=DES_PIXEL_SCALE

    def check_valid(self, info):
        n_image = len(info)
        ID = info.meta['row_id']
        stamp_size = info.meta['stamp_size']
        #Skip objects too big
        if stamp_size>self.options.max_stamp_size:
            raise I3StampTooLarge("Object stamp_size too big %d > %d"% (stamp_size, self.options.max_stamp_size))

        #Error checks - we have a max exposures limits.
        #And need at least one exposure
        #This is probably a supernova field that we do not
        #want to do here anyway
        if n_image>py3shape.structs.MAX_N_EXPOSURE:
            message = "OBJECT SEXTRACTOR ID %d HAS %d EXPOSURES IN FILE %s - MORE THAN MAX OF %d." % (ID, n_image, self._filename, py3shape.structs.MAX_N_EXPOSURE)
            raise I3TooManyExposures(message)

        #And until im5shape comes out we need at least 
        #some data to measure the shape of
        if n_image==0:
            message = "OBJECT WITH SEXTRACTOR ID %d HAS NO EXPOSURES" % (ID)
            raise I3NoExposures(message)


        #and at least some data has to have some weight to it
        weight_sum = sum(weight.sum() for weight in info.all('weight'))
        if weight_sum==0:
            raise I3NoWeight("No weight in any pixels")

    def preprocess(self, info, options):
        return {}



    def run(self, info, options):

        # set the image size
        stamp_size = info[0]['image'].shape[0]
        options.stamp_size = stamp_size

        #Finally running im3shape!
        #Any errors get caught higher up
        #We have an option to cheat 
        #and use some pre-computed results.
        #This is useful if we are re-postprocessing our object
        bands = info.meta['bands']
        result, best_img, images, weights = py3shape.analyze_multiexposure(
                info.all('image'), info.all('psf'), info.all('weight'),
                info.all('transform'), options, ID=info.meta['ID'], bands=bands)

        #Pull out the seperate models for each exposure
        models = np.split(best_img, len(images), axis=1)

        #Record the separate models.  Because the analyis
        #can also modify the image and weight, by scaling
        #it and removing backgrounds, we also replace that
        for exposure,model,image,weight in zip(info, models, images, weights):
            exposure['model'] = model
            exposure['image'] = image
            exposure['weight'] = weight

        return result



    @staticmethod
    def get_edge_gradients(image, weight):
        x=np.arange(image.shape[0])

        w=weight[0,:]
        x1=x[w>0]
        edge1=image[0,:][w>0]
        w=weight[-1,:]
        x2=x[w>0]
        edge2=image[-1,:][w>0]
        x=np.concatenate([x1,x2])
        edge=np.concatenate([edge1,edge2])
        p1 = np.polyfit(x, edge, 1)

        w=weight[:,0]
        x1=x[w>0]
        edge1=image[:,0][w>0]
        w=weight[:,-1]
        x2=x[w>0]
        edge2=image[:,-1][w>0]
        x=np.concatenate([x1,x2])
        edge=np.concatenate([edge1,edge2])
        p2 = np.polyfit(x, edge, 1)

        m1=p1[0]
        m2=p2[0]

        return m1, m2

    def postprocess_epoch(self, fitInfo, epoch_results, epoch_info):
        image = epoch_info['image']
        model = epoch_info['model']
        weight = epoch_info['weight']
        uber = epoch_info['uber_mask']
        psf = epoch_info['psf']

        epoch_results.update(fitInfo.measure(psf, image, model, weight, uber))


    def postprocess(self, result, info):
        n_exposure = len(info)
        nparam = count_varied_params(self.options)        
        main, epoch = result.as_dict(n_exposure, nparam)
        main['n_exposure'] = n_exposure
        main['nparam_varied'] = nparam
        main['stamp_size'] = info[0]['image'].shape[0]

        try:
            fitInfo = ModelFitInformation(result, self.options)
        except Exception as e:
            #raise the exception with a new class but a different 
            raise I3PostprocessModelFitError, str(e), sys.exc_info()[2]

        #Epoch-specific results
        for epoch_results, epoch_info in zip(epoch,info):
            # Flux fractions
            self.postprocess_epoch(fitInfo, epoch_results, epoch_info)

        self.get_mean_columns(main, epoch)
        return main, epoch



    def get_mean_columns(self, main, epoch):
        mean_cols=[
            'rgpp_rp',
            'psf_e1_sky',
            'psf_e2_sky',
            'psf_fwhm',
            'unmasked_flux_frac',
            'model_edge_mu',
            'model_edge_sigma',
            'edge_mu',
            'edge_sigma',
            'hsm_psf_e1_sky',
            'hsm_psf_e2_sky', 
            'hsm_psf_sigma',
            'hsm_psf_rho4',
            'mask_fraction',
        ]

        for col in mean_cols:
            v = np.array([e[col] for e in epoch])
            main['mean_'+col] = np.nanmean(v)
            main['fails_'+col] = nancount(v)

        #We also have two quadratic sum SNR measurements.
        for col in ['round_snr', 'round_snr_mw']:
            v = np.array([e[col] for e in epoch])**2
            main[col] = np.sqrt(np.nansum(v))
            main['fails_'+col] = nancount(v)







class I3MedsAnalysis(I3Analysis):
    def __init__(self, ini):
        options=py3shape.Options(ini)
        super(I3MedsAnalysis, self).__init__(options)

    def select_output(self, output, main_header, epoch_header):
        if self.options.database_output:
            if isinstance(output, DatabaseOutput):
                output = output
            else:
                output = DatabaseOutput(output, hostname=self.options.database_host, database=self.options.database_name)
        else:
            if isinstance(output,TextOutput):
                output=output
            else:
                output = TextOutput(output+".main.txt", output+".epoch.txt",main_header, epoch_header)
        return output

    def main(self, meds_files, ids, output, n_split, i_split, fatal_errors=False):
        self.options.validate()

        M = [I3MEDS(meds_file, self.options) for meds_file in meds_files]
        output = self.select_output(output, [], [])

        #If unspecfied, do all the meds files
        if ids is None:
            ids = np.unique(np.concatenate([m['id'] for m in M]))

        #split by process
        ids = np.array_split(ids, n_split)[i_split]


        for ID in ids:
            output.try_write_buffered_errors()
            try:
                
                self.active_meds_files = []
                infos = []
                meds_count = 0
                tstart = time.time()
                for m in M:
                    #find the ID in this meds file, if present
                    iobj = np.searchsorted(m['id'], ID)
                    if iobj<m.size and m['id'][iobj]==ID:
                        #Get the inputs for this meds file
                        info = m.get_im3shape_inputs(iobj)
                        #for each exposure put the corresponding meds file
                        #in the list
                        infos.append(info)
                        meds_count+=1
                        for i in xrange(len(info)):
                            self.active_meds_files.append(m)

                if self.options.verbosity>0:
                    print "Found {} meds files with ID {} in".format(meds_count, ID)
                    #print "Bands:", "".join(info['bands'] for info in infos)

                #Object not found anywhere - allow for unusual ID lists
                if not infos:
                    if self.options.verbosity>0:
                        print " [so skipping]"
                    continue

                #Combine all the infos together into one
                info = I3MultiExposureInputs.combine(infos)
                if self.options.verbosity>0:
                    print " with total {} exposures: {}".format(len(info), info.meta['bands'])

                #Collect the information together into a single list
                self.check_valid(info)
                extra = self.preprocess(info, self.options)

                result = self.run(info, self.options)
                main, epoch = self.postprocess(result, info)
                main.update(extra)
                time_taken = time.time() - tstart
                main['time'] = time_taken
                output.write_row(main, epoch)
            except KeyboardInterrupt:
                raise
            #Noteworthy error indicating structural or parameter
            #problem, not just with one galaxy.  Full traceback:
            except I3NoteworthyError as error:
                sys.stderr.write("NOTEWORTHY ERROR!\n")
                if fatal_errors:
                    raise
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                traceback.print_exc()
                output.write_failure(ID, error.i3_code)
            #Regular error, no need for a traceback:
            except I3GalaxyError as error:
                if fatal_errors:
                    raise
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                sys.stderr.write("Exception on line {} of file {}: {}\n".format(exc_tb.tb_lineno, fname, error))
                output.write_failure(ID, error.i3_code)
            #Unknown error that we do not expect - full traceback
            except Exception as error:
                print "WARNING NON-UNDERSTOOD ERROR:"
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                traceback.print_exc()
                if fatal_errors:
                    raise
                output.write_failure(ID, I3_UNKNOWN_ERROR)
            output.flush()

        #Finally - try to write any remaining errors, in case the last job
        output.try_write_buffered_errors()
        if not output.started and output.buffered_errors:
            sys.stderr.write("CRITICAL ERROR: All jobs in a task failed and so their errors could not be written.\n")
    

    def preprocess(self, info, options):
        extra = {}
        extra['tilename'] = info.meta['tilename']
        extra['bands'] = info.meta['bands']
        if 'ra' in info.meta:
            extra['ra'] = info.meta['ra']
            extra['dec'] = info.meta['dec']

        return extra



    def postprocess(self, result, info):
        """
        This is the same as the superclass one except that
        we update the active meds file each time

         """
        n_exposure = len(info)
        nparam = count_varied_params(self.options)        
        main, epoch = result.as_dict(n_exposure, nparam)
        main['n_exposure'] = n_exposure
        main['nparam_varied'] = nparam
        main['stamp_size'] = info[0]['image'].shape[0]

        fitInfo = ModelFitInformation(result, self.options)

        #Epoch-specific results
        for epoch_results, epoch_info, meds_file in zip(epoch,info,self.active_meds_files):
            self.active_meds_file = meds_file
            # Flux fractions
            self.postprocess_epoch(fitInfo, epoch_results, epoch_info)

        self.get_mean_columns(main, epoch)
        return main, epoch

    def postprocess_epoch(self, fitInfo, epoch_results, epoch_info):
        super(I3MedsAnalysis, self).postprocess_epoch(fitInfo, epoch_results, epoch_info)
        g1 = epoch_results["hsm_psf_e1"]
        g2 = epoch_results["hsm_psf_e2"]
        g1sky, g2sky = self.active_meds_file.convert_g_image2sky(
            epoch_info['iobj'], epoch_info['iexp'], self.options.stamp_size, 
            g1, g2)
        epoch_results["hsm_psf_e1_sky"] = g1sky
        epoch_results["hsm_psf_e2_sky"] = g2sky

        g1 = epoch_results["psf_e1"]
        g2 = epoch_results["psf_e2"]
        g1sky, g2sky = self.active_meds_file.convert_g_image2sky(
            epoch_info['iobj'], epoch_info['iexp'], self.options.stamp_size, 
            g1, g2)
        epoch_results["psf_e1_sky"] = g1sky
        epoch_results["psf_e2_sky"] = g2sky

        for col in ['orig_row', 'orig_col', 'exposure_name', 'ccd']:
            epoch_results[col] = epoch_info[col]



description = "Run im3shape on a meds image using multiple exposures fit."
parser = argparse.ArgumentParser(description=description, add_help=True)
parser.add_argument('meds', type=str, nargs='+', help='the input meds FITS file(s)')
parser.add_argument('ini', type=str, help='the name of the existing ini file to be used with im3shape')
parser.add_argument('cat', type=str, help='name of the catalog file containing the list of galaxies to run on.  First col should be row index. Use "all" to run on all objects.')
parser.add_argument('output', type=str, help='name of the results file with im3shape output in sky coordinates (one line per galaxy)')
parser.add_argument('index', type=int, help='Index of this process')
parser.add_argument('n_split', type=int, help='Number of processes to split over')
parser.add_argument('--fatal-errors', action='store_true', help='Number of entries to run')


def load_ids(cat):
    if cat=="all":
        IDs = None
    else:
        IDs = np.loadtxt(cat)
        if IDs.ndim==2:
            IDs = IDs[:, 0]
        IDs = IDs.astype(int)
    return IDs    

def main(meds_files, ini, cat, output, n_split, index, fatal_errors):
    analysis = I3MedsAnalysis(ini)
    IDs = load_ids(cat)
    return analysis.main(meds_files, IDs, output, n_split, index, fatal_errors)


if __name__ == '__main__':
    args = parser.parse_args()
    main(args.meds, args.ini, args.cat, args.output, args.n_split, args.index, args.fatal_errors)
