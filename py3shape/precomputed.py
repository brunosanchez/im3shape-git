"""
Code to get results from an external source instead
of running im3shape

"""
import numpy as np
from .results import Results
from . import utils
import pyfits
import glob
import os
from .errors import *

class FitsCatDirectory(object):
	def __init__(self, dirname):
		files = glob.glob(dirname+"/*.fits")
		self.files = {}
		for filename in files:
			tile = self.get_file_identifier(filename)
			self.files[tile] = filename
		self.current_hdu = None
		self.current_tile = ""
		self.current_ids = None

	def get_file_identifier(self, filename):
		return utils.find_tilename(filename)

	def open(self, tile):
		filename = self.files.get(tile)
		if filename is None:
			raise I3MissingPrecomputedResults("No precomputed tile results for %s"%tile)
		self.current_hdu = pyfits.open(filename)[1]
		self.current_ids = self.current_hdu.data['coadd_objects_id']
		self.current_tile = tile

	def lookup(self, filename, ID):
		#if current file is not the same as new tile
		#then find the precomputed results for the new
		#tile
		tile = self.get_file_identifier(filename)
		if tile != self.current_tile:
			self.open(tile)

		#look for ID in current_ids
		n = self.current_ids.searchsorted(ID)
		assert self.current_ids[n]==ID, "ID %r not found in output" % ID
		return self.current_hdu.data[n]



class GreatDesCatDirectory(FitsCatDirectory):
	def get_file_identifier(self, filename):
		#typical form of both meds and results file is
		#nbc.meds.110.g01.fits
		#so just use everything up to the .fits
		return os.path.splitext(os.path.split(filename)[1])[0]
