#ifndef UTILS_H
#define UTILS_H

#include "defs.h"

void readData(char*,matrix);
double timeDiff(struct timeval, struct timeval);
void writeDataText(matrix,char*);
#endif
